﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Contexts;
using Entities;

namespace DBSeedTester
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new MedicalInventoryAppContext())
            {

                Console.WriteLine("Initializing database.");
                //Load all users.
                IEnumerable<User> users = context.Set<User>().Include(x => x.MedicalChest).ToList();
                Console.WriteLine("Database initialized.");


                Console.WriteLine(".");

                //Load all medical chests.
                IEnumerable<MedicalChest> medicalChests =
                    context.Set<MedicalChest>()
                        .Include(x => x.User)
                        .Include(x => x.MedicalChestInformation)
                        .Include(x => x.ItemGroups).ToList();

                Console.WriteLine(".");

                //Load all medical chest informations.
                IEnumerable<MedicalChestInformation> medicalChestInformations =
                    context.Set<MedicalChestInformation>()
                        .Include(x => x.MedicalChestInformationLangs)
                        .Include(x => x.ItemGroupsInformations)
                        .Include(x => x.MedicalChests).ToList();
                Console.WriteLine(".");

                //Load all item groups.
                IEnumerable<ItemGroup> itemGroups =
                    context.Set<ItemGroup>()
                        .Include(x => x.ItemGroupInformation)
                        .Include(x => x.MedicalChest)
                        .Include(x => x.Items).ToList();
                Console.WriteLine(".");

                //Load all item groups information.
                IEnumerable<ItemGroupInformation> itemGroupInformations =
                    context.Set<ItemGroupInformation>()
                        .Include(x => x.ItemGroupInformationLangs)
                        .Include(x => x.ItemGroups)
                        .Include(x => x.ItemInformations)
                        .Include(x => x.MedicalChestInformation)
                        .ToList();
                Console.WriteLine(".");

                //Load all items.
                IEnumerable<ItemInformation> itemInformations =
                    context.Set<ItemInformation>()
                        .Include(x => x.ItemInformationLangs)
                        .Include(x => x.ItemGroupInformation)
                        .ToList();
                Console.WriteLine("Finished reading.");


                IEnumerable<ItemGroupInformation> list = context.Set<ItemGroupInformation>().
                    Include(itemGroupInformation => itemGroupInformation.ItemGroupInformationLangs)
                .Include(ItemGroupInformation => ItemGroupInformation.ItemInformations).
                Include(itemGroupInformation => itemGroupInformation.ItemInformations.Select(a => a.ItemGroupInformation)).ToList();

                IEnumerable<MedicalChest> medChest = context.Set<MedicalChest>()
                    .Include(x => x.ItemGroups)
                    .Include(y => y.ItemGroups.Select(a => a.Items))
                    .Include(x => x.ItemGroups.Select(a => a.Items.Select(I => I.ItemInformation.ItemInformationLangs))).ToList();


                context.Set<ItemGroupInformation>().Remove(((List<ItemGroupInformation>) itemGroupInformations)[0]);

                context.SaveChanges();

                itemGroupInformations =
                   context.Set<ItemGroupInformation>()
                       .Include(x => x.ItemGroupInformationLangs)
                       .Include(x => x.ItemGroups)
                       .Include(x => x.ItemInformations)
                       .Include(x => x.MedicalChestInformation)
                       .ToList();
                Console.WriteLine(".");

                
                int i = 0;


            }
        }
    }
}
