﻿namespace ExcelToXml.Entities
{
    public class ItemAmount
    {
        public int ID { get; set; }
        public double Amount { get; set; }
        public double Concentration { get; set; }
        public string Unit { get; set; }

        public double TotalAmount()
        {
            return Amount * Concentration;
        }
    }
}
