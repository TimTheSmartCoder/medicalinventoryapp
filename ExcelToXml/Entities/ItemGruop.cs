﻿using System.Collections.Generic;
using System.Security.AccessControl;
using System.Threading;
using Entities;

namespace ExcelToXml.Entities
{
    public class ItemGruop
    {
        private string groupNumber;
        public int ID { get; set; }
        public string GroupNumber
        {
            get { return groupNumber; }
            set
            {
                if (value.EndsWith("."))
                {
                    value = value.Substring(0, 1);
                }
                groupNumber = value;
            }
        }
        public string Name { get; set; }
        public List<AbstractItem> Items { get; set; }

        public ItemGruop()
        {
            Items = new List<AbstractItem>();
        }

        public bool ItemExists(AbstractItem item)
        {
            return Items.Exists(x => x.GroupNumber == item.GroupNumber);
        }

        
        
        
    }
}
