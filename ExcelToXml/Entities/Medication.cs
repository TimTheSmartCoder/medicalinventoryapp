﻿namespace ExcelToXml.Entities
{
    public class Medication : AbstractItem
    {
        public string AtcCode { get; set; }
        public string AdministrationForm { get; set; }
        public string Strength { get; set; }
        
    }
}
