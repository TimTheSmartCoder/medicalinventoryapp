﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelToXml.Entities
{
    public class ItemInstruction
    {
        public string Indication { get; set; }
        public string GroupNumber { get; set; }
        public string AtcCode { get; set; }
        public string PharmIngredients { get; set; }
        public string Form { get; set; }
        public string Effect { get; set; }
        public string Dosage { get; set; }
        public string SideEffects { get; set; }
        public string Validity { get; set; }
        public string Storage { get; set; }
        public string Remarks { get; set; }
    }
}
