﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelToXml.Entities
{
    public class Label
    {
        public string GroupNumber { get; set; }
        public string Form { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
    }
}
