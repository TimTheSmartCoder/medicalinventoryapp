﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelToXml.Entities
{
    public class Group
    {
        public int ItemCount { get; set; }
        public int CellCount { get; set; }
        public string GroupId { get; set; }

        public Group(int itemCount, string groupId)
        {
            ItemCount = itemCount;
            GroupId = groupId;

            SetCellCount();
        }

        private void SetCellCount()
        {
            char id = GroupId.ToCharArray()[0];
            CellCount = (Char.IsNumber(id)) ? 7 : 4;
        }
    }
}
