﻿namespace ExcelToXml.Entities
{
    public abstract class AbstractItem
    {
        
        public int ID { get; set; }

        public string GroupNumber
        {
            get; set;
        }

        public string Name { get; set; }
        public string ItemAmount { get; set; }
        public string Mfag { get; set; }
        

        public string Unit { get; set; }
    }
}
