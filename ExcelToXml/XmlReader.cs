﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using ExcelToXml.Entities;

namespace ExcelToXml
{
    public class XmlReader
    {
        private int _medicalStartLineNumber = 42603;
        private int _medicalEndLineNUmber = 48174;
        private int _equipmentStartLineNumber = 54786;
        private int _equipmentEndLineNumber = 61666;
        private int _instructionStartLineNumber = 35012;
        private int _instructionEndLineNumber = 42153;
        private int _labelStartLineNumber = 74172;
        private int _labelEndLineNumber = 76414;
        private string _emptyCellId = "s214";

        private Group[] _groups = new[] {
            new Group(4, "1"), new Group(4, "2"), new Group(12,"3"),
            new Group(8,"4"), new Group(16,"5"), new Group(8,"6"),
            new Group(22,"7"), new Group(6,"8"), new Group(10,"9"),
            new Group(10, "10"), new Group(4, "11"), new Group(14, "12"),
            new Group(14, "13"), new Group(4, "14"), new Group(10, "15"),
            new Group(8, "16"), new Group(22, "A"), new Group(26, "B"),
            new Group(20, "C"), new Group(28, "D"), new Group(20, "E"),
            new Group(10, "F"), new Group(10, "G"), new Group(12, "H"),
            new Group(4, "I"), new Group(20, "J"), new Group(12, "K"),
            new Group(14, "L"), new Group(6, "M"), new Group(4, "N"),
            new Group(8, "O"),
        };

        int[] groupItemCounts = new[] { 2, 4, 12, 8, 16, 8, 22, 4, 10, 10, 4, 14, 14, 4, 10, 8 };

        public List<ItemGruop> Gruops { get; set; }
        public List<Medication> Medications { get; set; }
        public List<Equipment> Equipments { get; set; }
        public List<ItemInstruction> ItemInstructions { get; set; }
        public List<Label> Labels { get; set; }
        public string Filepath { get; set; }

        private string start = "Grp. ";


        public XmlReader(string filepath)
        {
            Filepath = filepath;
            Gruops = new List<ItemGruop>();
            Medications = new List<Medication>();
            Equipments = new List<Equipment>();
            ItemInstructions = new List<ItemInstruction>();
            Labels = new List<Label>();
            Read();
        }

        public void Read()
        {
            XmlTextReader reader = new XmlTextReader(Filepath);
            ItemGruop group = null;
            while (reader.ReadToFollowing("Row"))
            {
                reader.ReadToFollowing("Data");
                if (reader.NodeType != XmlNodeType.Text && reader.NodeType != XmlNodeType.None)
                {
                    String data = reader.ReadElementContentAsString();

                    if (IsGroupRow(data) &&
                        IsInLineRange(reader.LineNumber, _instructionStartLineNumber, _instructionEndLineNumber))
                    {
                        String groupIndicator = data.Substring(start.Length, 2).Trim();
                        int indicator = int.Parse(groupIndicator) - 1;
                        ReadInstructions(reader, groupItemCounts[indicator]);
                    }
                    if (IsGroupRow(data) &&
                        (IsInLineRange(reader.LineNumber, _medicalStartLineNumber, _medicalEndLineNUmber) ||
                         IsInLineRange(reader.LineNumber, _equipmentStartLineNumber, _equipmentEndLineNumber)))
                    {
                        String groupIndicator =
                            (StartsWithNumber(data.Substring(start.Length, data.Length - start.Length)))
                                ? data.Substring(start.Length, 2).Trim()
                                : data.Substring(start.Length, 1).Trim();
                        Group g = _groups.FirstOrDefault(x => x.GroupId == groupIndicator);
                        group = new ItemGruop()
                        {
                            GroupNumber = groupIndicator,
                            Name =
                                data.Substring(start.Length + groupIndicator.Length + 1,
                                    data.Length - start.Length - groupIndicator.Length - 1).Trim(),
                            Items = new List<AbstractItem>(ReadMedicationGroup(reader, g))
                        };
                        Gruops.Add(group);
                    }
                    
                }
            }
            ReadLabels();

        }

        private void ReadLabels()
        {
            XmlTextReader reader = new XmlTextReader(Filepath);

            while (reader.ReadToFollowing("Worksheet"))
            {
                reader.MoveToFirstAttribute();
                if (reader.Value == "Labels ")
                {
                    break;
                }
            }

            Label label = null;
            while (reader.ReadToFollowing("Row"))
            {
                if (reader.LineNumber < _labelEndLineNumber)
                {
                    label = new Label();
                    for (int i = 0; i < 3; i++)
                    {
                        reader.ReadToFollowing("Font");
                        string data = reader.ReadElementContentAsString();

                        switch (i)
                        {
                            case 0:
                                label.GroupNumber = data.Substring(0, 4).Trim();
                                label.Name = data.Substring(4).Trim();
                                break;
                            case 1:
                                label.Form = data;
                                break;
                            case 2:
                                label.Text = data;
                                break;
                        }
                    }
                    Labels.Add(label);
                }
                else
                {
                    return;
                }
            }


        }

        public void ReadInstructions(XmlTextReader reader, int groupItemCount)
        {

            for (int l = 0; l < groupItemCount; l++)
            {
                int[] cells = new[] { 4, 2, 2, 2, 3, 3, 2, 2 };
                string[] dataStrings = new string[cells.Sum()];
                int rows = cells.Length;

                int pointer = 0;
                for (int i = 0; i < rows; i++)
                {
                    reader.ReadToFollowing("Row");
                    for (int j = 0; j < cells[i]; j++)
                    {
                        reader.ReadToFollowing("Data");
                        dataStrings[pointer + j] = reader.ReadElementContentAsString().Trim();
                    }
                    pointer += cells[i];
                }
                ItemInstruction instruction = new ItemInstruction()
                {
                    Indication = dataStrings[0],
                    GroupNumber = dataStrings[1],
                    PharmIngredients = dataStrings[3],
                    Form = dataStrings[5],
                    Effect = dataStrings[7],
                    Dosage = dataStrings[9],
                    SideEffects = dataStrings[12],
                    AtcCode = dataStrings[13],
                    Validity = dataStrings[15],
                    Storage = dataStrings[17],
                    Remarks = dataStrings[19],


                };
                ItemInstructions.Add(instruction);
            }
        }

        public List<AbstractItem> ReadMedicationGroup(XmlTextReader reader, Group group)
        {
            List<AbstractItem> items = new List<AbstractItem>();
            for (int i = 0; i < group.ItemCount; i++)
            {
                string[] dataArray = new string[group.CellCount];
                reader.ReadToFollowing("Row");
                for (int j = 0; j < group.CellCount; j++)
                {
                    reader.ReadToFollowing("Data");
                    dataArray[j] = reader.ReadElementContentAsString().Trim();
                }

                AbstractItem toAdd = null;
                if (StartsWithNumber(group.GroupId))
                {
                    toAdd = new Medication()
                    {
                        GroupNumber = dataArray[0],
                        AtcCode = dataArray[1],
                        Name = dataArray[2],
                        AdministrationForm = dataArray[3],
                        Strength = dataArray[4],
                        ItemAmount = dataArray[5],
                        Mfag = dataArray[6],
                    };
                    Medications.Add((Medication)toAdd);
                }
                else
                {
                    toAdd = new Equipment()
                    {
                        GroupNumber = dataArray[0],
                        Name = dataArray[1],
                        ItemAmount = dataArray[2],
                        Mfag = dataArray[3]
                    };
                    Equipments.Add((Equipment)toAdd);
                }
                items.Add(toAdd);

            }
            return items;
        }

        private bool IsGroupRow(string data)
        {
            return data.StartsWith(start) && !data.StartsWith(start + "No.");
        }

        public void AddItemToGroup(AbstractItem item)
        {
            foreach (var itemGruop in Gruops)
            {
                if (item.GroupNumber != null)
                {
                    string[] data = item.GroupNumber.Split(Convert.ToChar("."));
                    if (data[0].Equals(itemGruop.GroupNumber))
                    {
                        itemGruop.Items.Add(item);
                    }
                }
            }
        }

        private bool StartsWithLetter(string s)
        {
            return Char.IsLetter(s.ToCharArray()[0]);
        }
        private bool StartsWithNumber(string s)
        {
            return Char.IsNumber(s.ToCharArray()[0]);
        }

        private bool IsInLineRange(int current, int start, int end)
        {
            if (current >= start)
            {
                if (current < end)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
