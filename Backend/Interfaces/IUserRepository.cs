﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace Backend.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        /// <summary>
        /// Gets a user entity with the given username.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        User GetFromUserName(string userName);

        /// <summary>
        /// Gets a user entity with the given email.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        User GetFromEmail(string email);
    }
}
