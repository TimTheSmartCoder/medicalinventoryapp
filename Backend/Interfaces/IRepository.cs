﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace Backend
{
    public interface IRepository<TEntity> where TEntity : AbstractEntity
    {
        /// <summary>
        /// Get the entity with the given id.
        /// </summary>
        /// <param name="id">Id of entity to return.</param>
        /// <returns></returns>
        TEntity Get(int id);

        /// <summary>
        /// Get all entities.
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> GetAll();

        /// <summary>
        /// Create the given entity.
        /// </summary>
        /// <param name="entity">The entity to create.</param>
        /// <returns></returns>
        TEntity Create(TEntity entity);

        /// <summary>
        /// Updates the given entity.
        /// </summary>
        /// <param name="entity">The entity to update.</param>
        /// <returns></returns>
        TEntity Update(TEntity entity);

        /// <summary>
        /// Deletes the given entity.
        /// </summary>
        /// <param name="entity">The entity to delete.</param>
        /// <returns></returns>
        TEntity Delete(TEntity entity);

        /// <summary>
        /// Pagination method, to get the data in pages, to
        /// prevent loading all the data.
        /// </summary>
        /// <param name="pageIndex">Index of the paged data.</param>
        /// <param name="pageSize">Size of a page.</param>
        /// <returns></returns>
        IEnumerable<TEntity> Pagination(int pageIndex, int pageSize);

        /// <summary>
        /// Counts all the entities, there exists in the database
        /// for the specific entity.
        /// </summary>
        /// <returns></returns>
        int Count();
    }
}
