﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Contexts;
using Backend.Exceptions;
using Entities;

namespace Backend.Repositories
{
    class UserRepository : AbstractRepository<User>, IUserRepository
    {
        protected override IQueryable<User> Include(DbSet<User> set)
        {
            return set.Include(x => x.MedicalChest);
        }

        public override User Create(User entity)
        {
            if (!this.GetAll().Any())
                entity.Admin = true;

            return base.Create(entity);
        }

        public User GetFromUserName(string userName)
        {
            using (var context = new MedicalInventoryAppContext())
            {
                User entity = this.Include(context.Set<User>()).FirstOrDefault(e => e.UserName.Equals(userName));

                if (entity == null)
                    throw new DoNotExistException($"The entity witht the id of: {userName} do not exist.");

                return entity;
            }
        }

        public User GetFromEmail(string email)
        {
            using (var context = new MedicalInventoryAppContext())
            {
                User entity = this.Include(context.Set<User>()).FirstOrDefault(e => e.Email.Equals(email));

                if (entity == null)
                    throw new DoNotExistException($"The entity witht the id of: {email} do not exist.");

                return entity;
            }
        }
    }
}
