﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace Backend.Repositories
{
    class ItemRepository : AbstractRepository<Item>
    {
        protected override IQueryable<Item> Include(DbSet<Item> set)
        {
            return set
                .Include(item => item.ItemInformation)
                .Include(item => item.ItemInformation.ItemInformationLangs);
        }
    }
}
