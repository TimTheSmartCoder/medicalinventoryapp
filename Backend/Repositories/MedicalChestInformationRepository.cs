﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Contexts;
using Entities;

namespace Backend.Repositories
{
    class MedicalChestInformationRepository : AbstractRepository<MedicalChestInformation>
    {
        protected override IQueryable<MedicalChestInformation> Include(DbSet<MedicalChestInformation> set)
        {
            return set
                .Include(medicalChestInfromation => medicalChestInfromation.ItemGroupsInformations)
                .Include(
                    medicalChestInformation =>
                        medicalChestInformation.ItemGroupsInformations.Select(
                            itemGroupInformation => itemGroupInformation.ItemInformations))
                .Include(medicalChestInfromation => medicalChestInfromation.MedicalChestInformationLangs)
                .Include(medicalChestInfromation => medicalChestInfromation.MedicalChests);
        }

        protected override void AttachUpdate(MedicalInventoryAppContext context, MedicalChestInformation entity)
        {
            foreach (var medicalChestInformationLang in entity.MedicalChestInformationLangs)
            {
                context.Entry(medicalChestInformationLang).State = EntityState.Modified;
            }
        }

        protected override void AttachCreate(MedicalInventoryAppContext context, MedicalChestInformation entity)
        {
            base.AttachCreate(context, entity);
        }
    }
}
