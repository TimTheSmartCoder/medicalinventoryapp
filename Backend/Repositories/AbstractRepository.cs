﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Backend.Contexts;
using Backend.Exceptions;
using Entities;

namespace Backend.Repositories
{
    abstract class AbstractRepository<TEntity> : IRepository<TEntity> where TEntity : AbstractEntity
    {
        public virtual TEntity Get(int id)
        {
            using (var context = new MedicalInventoryAppContext())
            {
                TEntity entity = this.Include(context.Set<TEntity>()).FirstOrDefault(e => e.Id == id);

                if (entity == null)
                    throw new DoNotExistException($"The entity witht the id of: {id} do not exist.");

                return entity;
            }
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            using (var context = new MedicalInventoryAppContext())
            {
                return this.Include(context.Set<TEntity>()).ToList();
            }
        }

        public virtual TEntity Create(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            if (entity.Id != AbstractEntity.DefaultId)
                throw new AlreadyExistException(nameof(entity));

            using (var context = new MedicalInventoryAppContext())
            {
                this.AttachCreate(context, entity);

                context.Set<TEntity>().Add(entity);
                context.SaveChanges();
            }

            return entity;
        }

        public virtual TEntity Update(TEntity entity)
        {
            if (entity == null)            
                throw new ArgumentNullException(nameof(entity));

            using (var context = new MedicalInventoryAppContext())
            {
                TEntity entityTemp = context.Set<TEntity>().AsNoTracking().FirstOrDefault(x => x.Id == entity.Id);
                if (entityTemp == null)
                    throw new DoNotExistException($"The following entity with id of {entity.Id} do not exist");
                
                context.Entry(entity).State = EntityState.Modified;
                this.AttachUpdate(context, entity);

                context.SaveChanges();
            }
            return entity;
       ; }

        public virtual TEntity Delete(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            using (var context = new MedicalInventoryAppContext())
            {
                TEntity entityTemp = this.Include(context.Set<TEntity>()).FirstOrDefault(e => e.Id == entity.Id);

                if (entityTemp == null)
                    throw new DoNotExistException($"The following entity with id of {entity.Id} do not exist");
                
                context.Set<TEntity>().Remove(entityTemp);
                context.SaveChanges();
            }
            return entity;
        }

        public virtual IEnumerable<TEntity> Pagination(int pageIndex, int pageSize)
        {
            using (var context = new MedicalInventoryAppContext())
            {
                int count = context.Set<TEntity>().Count();

                //The maximum page index.
                int maxIndex = (count < pageSize) ? 1 : count / pageSize;

                if (pageIndex <= 0 || pageIndex > maxIndex)
                    throw new DoNotExistException($"Please make sure the pagination is between {0} and {maxIndex}.");

                return context.Set<TEntity>().OrderBy(entity => entity.Id).Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList();
            }
        }

        public int Count()
        {
            using (var context = new MedicalInventoryAppContext())
            {
                return this.Include(context.Set<TEntity>()).Count();
            }
        }

        protected virtual void AttachCreate(MedicalInventoryAppContext context, TEntity entity) { }
        protected virtual void AttachUpdate(MedicalInventoryAppContext context, TEntity entity) { }
        protected virtual void AttachDelete(MedicalInventoryAppContext context, TEntity entity) { }

        protected abstract IQueryable<TEntity> Include(DbSet<TEntity> set);
    }
}
