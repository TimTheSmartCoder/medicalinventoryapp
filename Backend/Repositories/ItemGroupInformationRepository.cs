﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Contexts;
using Entities;

namespace Backend.Repositories
{
    class ItemGroupInformationRepository : AbstractRepository<ItemGroupInformation>
    {
        protected override IQueryable<ItemGroupInformation> Include(DbSet<ItemGroupInformation> set)
        {
            return set
                .Include(itemGroupInformation => itemGroupInformation.ItemGroupInformationLangs)
                .Include(ItemGroupInformation => ItemGroupInformation.ItemInformations)
                .Include(
                    itemGroupInformation => itemGroupInformation.ItemInformations.Select(a => a.ItemGroupInformation))
                .Include(
                    ItemGroupInformation => ItemGroupInformation.ItemInformations.Select(a => a.ItemInformationLangs))
                .Include(itemGroupInformation => itemGroupInformation.MedicalChestInformation)
                .Include(itemGroupInformation => itemGroupInformation.ItemGroups);
        }

        protected override void AttachUpdate(MedicalInventoryAppContext context, ItemGroupInformation entity)
        {
            foreach (var itemGroupInformationLang in entity.ItemGroupInformationLangs)
            {
                context.Entry(itemGroupInformationLang).State = EntityState.Modified;
            }
        }

        protected override void AttachCreate(MedicalInventoryAppContext context, ItemGroupInformation entity)
        {
            context.Set<MedicalChestInformation>().Attach(entity.MedicalChestInformation);
        }

        public override ItemGroupInformation Create(ItemGroupInformation entity)
        {
            base.Create(entity);

            using (MedicalInventoryAppContext context = new MedicalInventoryAppContext())
            {
                ItemGroupInformation itemGroupInformation =
                    context.Set<ItemGroupInformation>()
                        .Include(x => x.MedicalChestInformation
                        .MedicalChests.Select(a => a.ItemGroups))
                        .FirstOrDefault(x => x.Id == entity.Id);

                if (itemGroupInformation == null)
                    return entity;

                foreach (var chest in itemGroupInformation.MedicalChestInformation.MedicalChests)
                {
                    chest.ItemGroups.Add(new ItemGroup() { ItemGroupInformation = itemGroupInformation});
                }

                context.Entry(itemGroupInformation.MedicalChestInformation).State = EntityState.Modified;
                context.SaveChanges();
            }

            return entity;
        }
    }
}
