﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Contexts;
using Backend.Exceptions;
using Entities;
using System.Diagnostics;

namespace Backend.Repositories
{
    class MedicalChestRepository : AbstractRepository<MedicalChest>
    {
        protected override IQueryable<MedicalChest> Include(DbSet<MedicalChest> set)
        {
            return set.Include(x => x.MedicalChestInformation.MedicalChestInformationLangs)
                .Include(x => x.ItemGroups)
                .Include(x => x.ItemGroups.Select(a => a.ItemGroupInformation))
                .Include(x => x.ItemGroups.Select(a => a.Items))
                .Include(x => x.ItemGroups.Select(a => a.Items.Select(b => b.ItemInformation)))
                .Include(
                    x => x.ItemGroups.Select(a => a.Items.Select(b => b.ItemInformation.ItemInformationLangs.Select(c => c.ItemInformations))))
                .Include(x => x.ItemGroups.Select(a => a.ItemGroupInformation.ItemGroupInformationLangs));

        }

        protected override void AttachCreate(MedicalInventoryAppContext context, MedicalChest entity)
        {
            context.Set<User>().Attach(entity.User);
        }

        public override MedicalChest Update(MedicalChest entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            using (var context = new MedicalInventoryAppContext())
            {
                MedicalChest entityTemp = context.Set<MedicalChest>().AsNoTracking().FirstOrDefault(x => x.Id == entity.Id);
                if (entityTemp == null)
                    throw new DoNotExistException($"The following entity with id of {entity.Id} do not exist");

                this.AttachUpdate(context, entity);
                context.SaveChanges();
            }
                

            return entity;
            
        }

        public override MedicalChest Create(MedicalChest entity)
        {
            /**
             * We override the default create method, because we want to construct the
             * MedicalChest, ItemGroup and Item entities to the user.
             */

            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            if (entity.MedicalChestInformation == null)
                throw  new ArgumentNullException(nameof(entity.MedicalChestInformation));
            if (entity.User == null)
                throw new ArgumentNullException(nameof(entity.User));

            //Get repository for MedicalChestInformation.
            IRepository<MedicalChestInformation>
                    mciRepository = new MedicalChestInformationRepository();

            //Get the MedicalChestInformation.
            MedicalChestInformation medicalChestInformation 
                = mciRepository.Get(entity.MedicalChestInformation.Id);

            using (var context = new MedicalInventoryAppContext())
            {
                IEnumerable<ItemGroupInformation> itemGroupInformation 
                    = medicalChestInformation.ItemGroupsInformations;

                IList<ItemGroup> itemGroups = new List<ItemGroup>();

                foreach (ItemGroupInformation groupInformation 
                    in itemGroupInformation)
                {
                    context.Set<ItemGroupInformation>().Attach(groupInformation);

                    IEnumerable<ItemInformation> itemInformations 
                        = groupInformation.ItemInformations;

                    ItemGroup itemGroup = new ItemGroup()
                    {
                        ItemGroupInformation = groupInformation,
                        Items = new List<Item>()
                    };

                    foreach (ItemInformation itemInformation 
                        in itemInformations)
                    {
                        context.Set<ItemInformation>().Attach(itemInformation);
                        Item item = new Item()
                        {
                            ItemInformation = itemInformation
                        };

                        itemGroup.Items.Add(item);
                    }

                    entity.ItemGroups.Add(itemGroup);
                }

                context.Set<MedicalChestInformation>().Attach(medicalChestInformation);
                
                entity.MedicalChestInformation = medicalChestInformation;

                this.AttachCreate(context, entity);
                context.Set<MedicalChest>().Add(entity);
                context.SaveChanges();
            }

            return entity;
        }

        protected override void AttachUpdate(MedicalInventoryAppContext context, MedicalChest entity)
        {
            //context.Set<User>().Attach(entity.User);
            foreach (ItemGroup group in entity.ItemGroups)
            {
                foreach (Item item in group.Items)
                {
                    item.ItemInformation = null;
                    item.ItemGroup = null;
                    context.Set<Item>().Attach(item);
                    context.Entry(item).State = EntityState.Modified;  
                }
            }

        }
    }
}
