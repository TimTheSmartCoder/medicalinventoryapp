﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Contexts;
using Entities;

namespace Backend.Repositories
{
    class ItemInformationRepository : AbstractRepository<ItemInformation>
    {
        protected override IQueryable<ItemInformation> Include(DbSet<ItemInformation> set)
        {
            return set
                .Include(itemInformation => itemInformation.ItemGroupInformation)
                .Include(itemInformation => itemInformation.ItemInformationLangs)
                .Include(itemInformation => itemInformation.Items);
        }

        protected override void AttachUpdate(MedicalInventoryAppContext context, ItemInformation itemInformation)
        {
            foreach (var informationLang in itemInformation.ItemInformationLangs)
            {
                context.Entry(informationLang).State = EntityState.Modified;
            }
        }

        protected override void AttachCreate(MedicalInventoryAppContext context, ItemInformation itemInformation)
        {
            context.Set<ItemGroupInformation>().Attach(itemInformation.ItemGroupInformation);
        }

        public override ItemInformation Create(ItemInformation entity)
        {
            base.Create(entity);

            //Create related items for users which already has selected the chest.
            using (MedicalInventoryAppContext context = new MedicalInventoryAppContext())
            {
                ItemInformation itemInformation =
                    context.Set<ItemInformation>()
                        .Include(x => x.ItemGroupInformation.ItemGroups.Select(a => a.Items))
                        .FirstOrDefault(x => x.Id == entity.Id);

                if (itemInformation == null)
                    return entity;
                
                foreach (var itemGroup in itemInformation.ItemGroupInformation.ItemGroups)
                {
                    itemGroup.Items.Add(new Item() {ItemInformation = itemInformation});
                }

                context.Entry(itemInformation.ItemGroupInformation).State = EntityState.Modified;
                context.SaveChanges();
            }

            return entity;
        }
    }
}
