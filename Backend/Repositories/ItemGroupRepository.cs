﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace Backend.Repositories
{
    class ItemGroupRepository : AbstractRepository<ItemGroup>
    {
        protected override IQueryable<ItemGroup> Include(DbSet<ItemGroup> set)
        {
            return set
                .Include(itemGroup => itemGroup.MedicalChest)
                .Include(itemGroup => itemGroup.ItemGroupInformation)
                .Include(itemGroup => itemGroup.Items)
                .Include(itemGroup => itemGroup.ItemGroupInformation.ItemInformations.Select(a => a.ItemGroupInformation));
        }
    }
}
