﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend.Repositories;
using Entities;

namespace Backend
{
    //TODO: Consider change to dynamic factory instead of using a facade.
    public class RepositoryFacade
    {
        public static IUserRepository GetUserManager()
        {
            return new UserRepository();
        }

        public static IRepository<Item> GetItemManager()
        {
            return new ItemRepository();
        }

        public static IRepository<ItemGroup> GetItemGroupRepository()
        {
            return new ItemGroupRepository();
        }

        public static IRepository<MedicalChest> GetMedicalChestManager()
        {
            return new MedicalChestRepository();
        }

        public static IRepository<MedicalChestInformation> GetMedicalChestInformationManager()
        {
            return new MedicalChestInformationRepository();
        }

        public static IRepository<ItemInformation> GetItemInformationManager()
        {
            return new ItemInformationRepository();
        }

        public static IRepository<ItemGroupInformation> GetItemGroupInformationManager()
        {
            return new ItemGroupInformationRepository();
        }
    }
}
