﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using ExcelToXml;
using ExcelToXml.Entities;

namespace Backend.Contexts
{
    internal class MedicalInventoryAppInitializer
        : DropCreateDatabaseAlways<MedicalInventoryAppContext>
    {
        delegate void AddToGroup(ItemInformation itemInformation, string groupNumber);
        protected override void Seed(MedicalInventoryAppContext context)
        {
            const string danish = "da-DK";
            const string english = "en";

            //HARDCODED ENTITIES BEGN.
            //ItemInformationLang danishItemInformationLang = new ItemInformationLang()
            //{
            //    Name = "Medicin",
            //    ItemAmount = "ItemAmount",
            //    Administration = "Administration",
            //    Mfag = "MFag",
            //    Locale = danish
            //};

            //ItemInformationLang englishItemInformationLang = new ItemInformationLang()
            //{
            //    Name = "Medicin",
            //    ItemAmount = "ItemAmount",
            //    Administration = "Administration",
            //    Mfag = "MFag",
            //    Locale = english
            //};


            //ItemInformation itemInformation = new ItemInformation()
            //{
            //    Unit = "Unit",
            //    AtcCode = "AtcCode",
            //    Strength = "Strength",
            //    ItemInformationLangs = new List<ItemInformationLang>()
            //    {
            //        danishItemInformationLang,
            //        englishItemInformationLang
            //    }
            //};

            //Item item = new Item()
            //{
            //    ItemInformation = itemInformation
            //};

            //ItemGroupInformationLang danishItemGroupInformationLang = new ItemGroupInformationLang()
            //{
            //    Name = "Name",
            //    Locale = danish
            //};

            //ItemGroupInformationLang englishItemGroupInformationLang = new ItemGroupInformationLang()
            //{
            //    Name = "Name",
            //    Locale = danish
            //};

            //ItemGroupInformation itemGroupInformation = new ItemGroupInformation()
            //{
            //    Name = "Name",
            //    GroupNumber = "GroupNumber",
            //    ItemInformations = new List<ItemInformation>()
            //    {
            //        itemInformation
            //    },
            //    ItemGroupInformationLangs = new List<ItemGroupInformationLang>()
            //    {
            //        danishItemGroupInformationLang,
            //        englishItemGroupInformationLang
            //    }
            //};

            //ItemGroup itemGroup = new ItemGroup()
            //{
            //    ItemGroupInformation = itemGroupInformation,
            //    Items = new List<Item>()
            //    {
            //        item
            //    }
            //};

            //MedicalChestInformationLang danishChestInformationLang = new MedicalChestInformationLang()
            //{
            //    Locale = danish
            //};

            //MedicalChestInformationLang englishChestInformationLang = new MedicalChestInformationLang()
            //{
            //    Locale = english
            //};

            //MedicalChestInformation medicalChestInformation = new MedicalChestInformation()
            //{
            //    MedicalChestInformationLangs = new List<MedicalChestInformationLang>()
            //    {
            //        danishChestInformationLang,
            //        englishChestInformationLang
            //    },
            //    ItemGroupsInformations = new List<ItemGroupInformation>()
            //    {
            //        itemGroupInformation
            //    }
            //};


            //MedicalChest medicalChest = new MedicalChest()
            //{
            //    MedicalChestInformation = medicalChestInformation,
            //    ItemGroups = new List<ItemGroup>()
            //    {
            //        itemGroup
            //    }
            //};

            //User user = new User()
            //{
            //    MedicalChest = medicalChest,
            //};

            //context.Set<User>().Add(user);
            //context.SaveChanges();
            //int i = 0;
            //HARDCODED ENTITIES END.

            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string path = "Data/xml-medchest.xml";
            string filepath = Path.Combine(baseDirectory, path);
            XmlReader reader = new XmlReader(filepath);
            var itemGruops = reader.Gruops;
            var equipments = reader.Equipments;
            var medications = reader.Medications;
            var labels = reader.Labels;

            List<ItemGroupInformation> itemGroups = new List<ItemGroupInformation>();
            List<ItemInformation> itemInformations = new List<ItemInformation>();
            MedicalChestInformation medicalChestInformation = new MedicalChestInformation();

            

            //internal method/delegate which adds an item to its corresponding group
            AddToGroup addToGroup = delegate (ItemInformation itemInformation, string groupNumber)
            {
                foreach (ItemGroupInformation itemGroup in itemGroups)
                {
                    string compare = groupNumber.Substring(0, 2);
                    compare = (compare.EndsWith(".")) ? compare.Substring(0, 1) : compare;
                    if (compare == itemGroup.GroupNumber)
                    {
                        itemGroup.ItemInformations.Add(itemInformation);
                    }
                }
            };
            //Create ItemGroup entities from old groups
            foreach (ItemGruop itemGruop in itemGruops)
            {
                string[] namesArray = itemGruop.Name.Split('/');
                ItemGroupInformation group = new ItemGroupInformation()
                {
                    GroupNumber = itemGruop.GroupNumber,
                    Name = itemGruop.Name,
                    ItemInformations = new List<ItemInformation>()
                };
                List<ItemGroupInformationLang> groupLangs = new List<ItemGroupInformationLang>();
                groupLangs.Add(new ItemGroupInformationLang() { Locale = danish, Name = namesArray[0].Trim() });
                groupLangs.Add(new ItemGroupInformationLang() { Locale = english, Name = namesArray[1].Trim() });
                group.ItemGroupInformationLangs = groupLangs;

                itemGroups.Add(group);
            }

            //initialization of equipment entities
            //for (int i = 0; i < equipments.Count; i++)
            //{
            //    //extract equipment entities in pairs
            //    Equipment equip1 = equipments.ElementAt(i);
            //    Equipment equip2 = equipments.ElementAt(++i);

            //    //create danish entity
            //    ItemInformation itemInformation = new ItemInformation()
            //    {
            //        AtcCode = String.Empty,
            //
            //        Strength = String.Empty,
            //        
            //    };
            //    List<ItemInformationLang> itemLangs = new List<ItemInformationLang>();
            //
            //    //create english language entity
            //    itemLangs.Add(new ItemInformationLang()
            //    {
            //        Name = equip1.Name,
            //        ItemAmount = equip1.ItemAmount,
            //        Mfag = equip1.Mfag,
            //        Unit = equip1.Unit,
            //        Administration = String.Empty,
            //        Locale = danish
            //    });
            //    itemLangs.Add(new ItemInformationLang()
            //    {
            //        Name = equip2.Name,
            //        ItemAmount = equip2.ItemAmount,
            //        Mfag = equip2.Mfag,
            //        Unit = equip2.Unit,
            //        Administration = String.Empty,
            //        Locale = english
            //    });
            //    itemInformation.ItemInformationLangs = itemLangs;
            //    itemInformations.Add(itemInformation);
            //    addToGroup(itemInformation, equip1.GroupNumber);
            //    itemInformations.Add(itemInformation);
            //}

            for (int i = 0; i < medications.Count; i++)
            {
                //extract medications in pairs
                Medication med1 = medications.ElementAt(i);
                Medication med2 = medications.ElementAt(++i);
                Label label = labels.FirstOrDefault(x => med1.GroupNumber.StartsWith(x.GroupNumber));

                //create danish entity
                ItemInformation itemInformation = new ItemInformation()
                {
                    AtcCode = med1.AtcCode,
                    Strength = med1.Strength

                };
                List<ItemInformationLang> itemLangs = new List<ItemInformationLang>();
                ItemInformationLang langDa = new ItemInformationLang()
                {
                    Name = med1.Name,
                    ItemAmount = med1.ItemAmount,
                    Mfag = med1.Mfag,
                    Administration = med1.AdministrationForm,
                    Unit = med1.Unit,
                    Locale = danish
                };
                //if (label != null)
                //{
                //    langDa.LabelGroupNr = label.GroupNumber;
                //    langDa.LabelName = label.Name;
                //    langDa.LabelForm = label.Form;
                //    langDa.LabelText = label.Text;
                //}

                itemLangs.Add(langDa);

                
                //create english language entity
                itemLangs.Add(new ItemInformationLang()
                {
                    Name = med2.Name,
                    ItemAmount = med2.ItemAmount,
                    Mfag = med2.Mfag,
                    Administration = med2.AdministrationForm,
                    Unit = med2.Unit,
                    Locale = english
                });
                itemInformation.ItemInformationLangs = itemLangs;
                itemInformations.Add(itemInformation);
                addToGroup(itemInformation, med1.GroupNumber);

            }

            medicalChestInformation.ItemGroupsInformations = itemGroups;
            medicalChestInformation.MedicalChestInformationLangs = new List<MedicalChestInformationLang>()
            {
                new MedicalChestInformationLang() { Locale = danish, Name = "Medicinkiste A MFAG A1"},
                new MedicalChestInformationLang() { Locale = english, Name = "Medicine Chest A MFAG A1"}
            };

            context.Set<MedicalChestInformation>().Add(medicalChestInformation);
            context.SaveChanges();
            base.Seed(context);
        }
    }

    public class MedicalInventoryAppContext : DbContext
    {
        public MedicalInventoryAppContext()
        {
            Database.SetInitializer<MedicalInventoryAppContext>(new MedicalInventoryAppInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            /**
             * Relationship between Information entities.
             */
            //Many to Many relationship.
            modelBuilder.Entity<MedicalChestInformation>()
               .HasMany(medicalChestInformation => medicalChestInformation.MedicalChestInformationLangs)
               .WithMany(medicalChestInformationLang => medicalChestInformationLang.MedicalChestInformations)
               .Map(mm =>
               {
                   mm.MapLeftKey("MedicalChestInformationRefId");
                   mm.MapRightKey("MedicalChestInformationLangRefId");
                   mm.ToTable("MedicalInformationMedicalInformationLang");
               });

            //Many-To-Many relationship between MedicalChestInformation and ItemGroupInformation.
            modelBuilder.Entity<MedicalChestInformation>()
                .HasMany(medicalChestInformation => medicalChestInformation.ItemGroupsInformations)
                .WithRequired(information => information.MedicalChestInformation)
                .Map(m => m.MapKey("ItemGroupInformationId"))
                .WillCascadeOnDelete(true);
                

            //Many-To-Many relationship between ItemInformation and ItemGroupInformation.
            //modelBuilder.Entity<ItemGroupInformation>()
            //    .HasMany(itemGroupInformation => itemGroupInformation.ItemInformations)
            //    .WithMany(itemInformation => itemInformation.ItemGroupInformations)
            //    .Map(mm =>
            //    {
            //        mm.MapLeftKey("ItemGroupInformationRefId");
            //        mm.MapRightKey("ItemInformationRefId");
            //        mm.ToTable("ItemGroupInformationItemInformation");
            //    });

            //modelBuilder.Entity<ItemGroupInformation>()
            //    .HasMany(itemGroupInformation => itemGroupInformation.ItemInformations)
            //    .WithRequired(itemInformation => itemInformation.ItemGroupInformation)
            //    .WillCascadeOnDelete(true);
            modelBuilder.Entity<ItemInformation>()
                .HasOptional(itemInformation => itemInformation.ItemGroupInformation)
                .WithMany(itemGroupInformation => itemGroupInformation.ItemInformations)
                .Map(m => m.MapKey("ItemGroupInformationId"))
                .WillCascadeOnDelete(true);

            /**
             * Relationship between InformatonLang entities.
             */

            //Many-To-Many relationship between MedicalChestInformation and MedicalChestInformationLang.
            modelBuilder.Entity<MedicalChestInformation>()
               .HasMany(medicalChestInformation => medicalChestInformation.MedicalChestInformationLangs)
               .WithMany(medicalChestInformationLang => medicalChestInformationLang.MedicalChestInformations)
               .Map(mm =>
               {
                   mm.MapLeftKey("MedicalChestInformationRefId");
                   mm.MapRightKey("MedicalChestInformationLangRefId");
                   mm.ToTable("MedicalChestInformationMedicalChestInformationLang");
               });

            //Many-To-Many relationship between ItemGroupInformation and ItemGroupInformationLang.
            modelBuilder.Entity<ItemGroupInformation>()
                .HasMany(itemGroupInformation => itemGroupInformation.ItemGroupInformationLangs)
                .WithMany(itemGroupInformationLang => itemGroupInformationLang.ItemGroupInformations)
                .Map(mm =>
                {
                    mm.MapLeftKey("ItemGroupInformationRefId");
                    mm.MapRightKey("ItemGroupInformationLangRefId");
                    mm.ToTable("ItemGroupInformationItemGroupInformationLang");
                });

            //Many-To-Many relationship between ItemInformation and ItemInformationLang.
            modelBuilder.Entity<ItemInformation>()
                .HasMany(itemInformation => itemInformation.ItemInformationLangs)
                .WithMany(itemInformationLang => itemInformationLang.ItemInformations)
                .Map(mm =>
                {
                    mm.MapLeftKey("ItemInfromationRefId");
                    mm.MapRightKey("ItemInformationLangRefId");
                    mm.ToTable("ItemInformationItemInformationLang");
                });

            /**
             * Relationship between regular and information entities.
             */

            modelBuilder.Entity<MedicalChest>()
                .HasOptional(medicalChest => medicalChest.MedicalChestInformation)
                .WithMany(medicalChestInformation => medicalChestInformation.MedicalChests)
                .Map(m => m.MapKey("MedicalChestInformationId"))
                .WillCascadeOnDelete(true);

            //One-To-Many relationship between ItemGroup and ItemGroupInformation.
            modelBuilder.Entity<ItemGroup>()
                .HasOptional(itemGroup => itemGroup.ItemGroupInformation)
                .WithMany(itemGroupInformation => itemGroupInformation.ItemGroups)
                .Map(m => m.MapKey("ItemGroupInformationRefId"))
                .WillCascadeOnDelete(true);

            //One-To-Many relationship between Item and ItemInformation.
            modelBuilder.Entity<Item>()
                .HasOptional(item => item.ItemInformation)
                .WithMany(itemInformation => itemInformation.Items)
                .Map(m => m.MapKey("ItemInformationId"))
                .WillCascadeOnDelete(true);

            /**
             * Relationship between regular entities.
             */

            //One-To-One relationship between User and MedicalChest.
            modelBuilder.Entity<User>()
                .HasOptional(user => user.MedicalChest)
                .WithRequired(medicalChest => medicalChest.User);

            //One-To-Many relationship between MedicalChest and ItemGroup.
            modelBuilder.Entity<MedicalChest>()
                .HasMany(medicalChest => medicalChest.ItemGroups)
                .WithOptional(itemGroup => itemGroup.MedicalChest)
                .Map(m => m.MapKey("ItemGroupId"));

            //One-To-Many relationship between ItemGroup and Item.
            modelBuilder.Entity<ItemGroup>()
                .HasMany(itemGroup => itemGroup.Items)
                .WithOptional(item => item.ItemGroup)
                .Map(m => m.MapKey("ItemId"));

            base.OnModelCreating(modelBuilder);

            //------

            ////One-To-Many relationship between User and MedicalChest.
            //modelBuilder.Entity<User>()
            //    .HasOptional(user => user.MedicalChest)
            //    .WithMany(medicalChest => medicalChest.Users);

            ////One-To-Many relationship between MedicalChest and MedicalChestInformation.
            //modelBuilder.Entity<MedicalChest>()
            //    .HasRequired(medicalChest => medicalChest.MedicalChestInformation)
            //    .WithMany(medicalChestInformation => medicalChestInformation.MedicalChests);

            ////One-To-Many relationship between Item and MedicalChest.
            //modelBuilder.Entity<Item>()
            //    .HasRequired(item => item.MedicalChest)
            //    .WithMany(medicalChest => medicalChest.Items);

            ////One-To-Many relationship between Item and ItemInformation.
            //modelBuilder.Entity<Item>()
            //    .HasRequired(item => item.ItemInformation)
            //    .WithMany(itemInformation => itemInformation.Items);

            //modelBuilder.Entity<ItemInformation>()
            //    .HasMany(itemInformation => itemInformation.ItemInformationLangs)
            //    .WithMany(itemInformationLang => itemInformationLang.ItemInformations)
            //    .Map(ii =>
            //    {
            //        ii.MapLeftKey("ItemInformationRefId");
            //        ii.MapRightKey("ItemInformationLangRefId");
            //        ii.ToTable("ItemInformtionItemInformationLang");
            //    });
        }

        public DbSet<User> Users { get; set; }

        public DbSet<MedicalChest> MedicalChests { get; set; }
        public DbSet<MedicalChestInformation> MedicalChestInformations { get; set; }
        public DbSet<MedicalChestInformationLang> MedicalChestInformationLangs { get; set; }

        public DbSet<ItemGroup> ItemGroups { get; set; }
        public DbSet<ItemGroupInformation> ItemGroupInformations { get; set; }
        public DbSet<ItemGroupInformationLang> ItemGroupInformationLangs { get; set; }

        public DbSet<Item> Items { get; set; }
        public DbSet<ItemInformation> ItemInformations { get; set; }
        public DbSet<ItemInformationLang> ItemInformationLangs { get; set; }
    }
}
