﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Exceptions
{
    [Serializable]
    public class AlreadyExistException : Exception
    {
        public AlreadyExistException(String message) : base(message) { }
        public AlreadyExistException(String message, Exception innerException) : base(message, innerException) { }
    }
}
