﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entities;

namespace Frontend.Models.ViewModels.Admin.MedicineGroup
{
    public class MedicineGroupInfoViewModel
    {
        public ItemGroupInformation ItemGroupInformation { get; set; }
    }
}