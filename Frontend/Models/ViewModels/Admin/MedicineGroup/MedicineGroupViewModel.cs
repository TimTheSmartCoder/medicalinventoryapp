﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entities;

namespace Frontend.Models.ViewModels.Admin.MedicineGroup
{
    public class MedicineGroupViewModel
    {
        public IEnumerable<ItemGroupInformation> ItemGroupInformations { get; set; }
    }
}