﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entities;

namespace Frontend.Models.ViewModels.Admin.MedicineChest
{
    public class MedicineChestViewModel
    {
        public IEnumerable<MedicalChestInformation> MedicalChestInformations { get; set; }
    }
}