﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frontend.Models.ViewModels.Admin.User
{
    public class UserViewModel
    {
        public IEnumerable<Entities.User> Users { get; set; }

        public int NumberOfPages { get; set; }
        
        public int CurrentPage { get; set; }
    }
}