﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frontend.Models.ViewModels.Admin.User
{
    public class UserInfoViewModel
    {
        public Entities.User User { get; set; }
    }
}