﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entities;

namespace Frontend.Models.ViewModels.Admin.Medicin
{
    public class MedicinViewModel
    {
        public IEnumerable<ItemInformation> ItemInformations { get; set; }
        public int NumberOfPages { get; set; }
    }
}