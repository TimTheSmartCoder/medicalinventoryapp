﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceGateway.Authentication;

namespace Frontend.Models.Authentication
{
    public class AuthenticationManager
    {
        private static string _authSessionName = "AuthSession";

        /// <summary>
        /// returns a IAuthStorage to the session for authentication to be stored in
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IAuthStorage GetStorage(HttpContextBase context)
        {
            if (context.Session[_authSessionName] == null)
                return (IAuthStorage) (context.Session[_authSessionName] = new DefaultAuthStorage());
            else
                return (IAuthStorage) context.Session[_authSessionName];
        }
    }
}