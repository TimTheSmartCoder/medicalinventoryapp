﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml;

namespace Frontend.Models.Authentication
{
    public class AuthorizeIsValidAttribute : ActionFilterAttribute
    {

        /// <summary>
        /// If the user fails to be authorized the user will be redirected to a default page specified underneath
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);


            if (!AuthenticationManager.GetStorage(filterContext.HttpContext).IsValid())
            {
                filterContext.Result =
                    new RedirectToRouteResult(new RouteValueDictionary(new {controller = "Auth", action = "UserLogin", url = filterContext.HttpContext.Request.Url.AbsoluteUri}));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

            filterContext.Controller.ViewBag.IsAuthenticated = true;
        }
    }
}