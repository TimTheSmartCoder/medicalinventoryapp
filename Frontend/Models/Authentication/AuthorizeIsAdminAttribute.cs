﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ServiceGateway;
using ServiceGateway.Services;
using Entities;

namespace Frontend.Models.Authentication
{
    public class AuthorizeIsAdminAttribute : ActionFilterAttribute
    {

        /// <summary>
        /// If the user fails to be authorized the user will be redirected to a default page specified underneath
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            DefaultServiceGateway gateway = new DefaultServiceGateway(AuthenticationManager.GetStorage(filterContext.HttpContext));

            IUserService userService = gateway.GetService<Entities.User, IUserService>();

            Entities.User user = userService.GetAuthenticatedUser();

            if (!user.Admin)
            {
                filterContext.Result =
                    new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Auth", action = "Index" }));
            }
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);

            filterContext.Controller.ViewBag.IsAdmin = true;
        }
    }
}