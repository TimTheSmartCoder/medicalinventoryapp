﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models.BindingModels.Admin
{
    public class MedicineChestCreateBindingModel
    {
        [Required]
        public string DanishName { get; set; }

        [Required]
        public string EnglishName { get; set; }
    }

    public class MedicineChestEditBindingModel : MedicineChestCreateBindingModel
    {
        [Required]
        public int Id { get; set; }
    }

    public class MedicineChestDeleteBindingModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}