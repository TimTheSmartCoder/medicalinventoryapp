﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend.Models.BindingModels.Admin
{
    public class UserEditBindingModel
    {
        [Required]
        public int Id { get; set; }
        
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }

    public class UserDeleteBindingModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string UserName { get; set; }
    }
}