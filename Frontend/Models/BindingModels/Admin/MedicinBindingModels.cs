﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Entities;

namespace Frontend.Models.BindingModels.Admin
{
    public class MedicinEditBindingModel
    {
        public int Id { get; set; }

        [Required]
        public string DanishName { get; set; }
        [Required]
        public string EnglishName { get; set; }
        [Required]
        public string AtcCode { get; set; }
        [Required]
        public string EnglishAdministration { get; set; }
        [Required]
        public string DanishAdministration { get; set; }
        [Required]
        public string DanishMFag { get; set; }
        [Required]
        public string EnglishMFag { get; set; }
    }

    public class MedicinCreateBindingModel : MedicinEditBindingModel
    {
        public int ItemGroup { get; set; }

        public IEnumerable<ItemGroupInformation> ItemGroupInformations { get; set; }
    }

    public class MedicinDeleteBindingModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}