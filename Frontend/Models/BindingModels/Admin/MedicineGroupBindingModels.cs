﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Entities;

namespace Frontend.Models.BindingModels.Admin
{
    public class MedicineGroupEditBindingModel
    {
        public int Id { get; set; }

        [Required]
        public string DanishName { get; set; }

        [Required]
        public string EnglishName { get; set; }
    }

    public class MedicineGroupCreateBindingModel : MedicineGroupEditBindingModel
    {
        public int MedicalChestInformation { get; set; }
        public IEnumerable<MedicalChestInformation> MedicalChestInformations { get; set; }
    }

    public class MedicineGroupDeleteBindingModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}