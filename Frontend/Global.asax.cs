﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Frontend.Models.BindingModels.Admin;

namespace Frontend
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //AutoMapper configuration begin.
            AutoMapper.Mapper.Initialize(config =>
            {
                config
                    .CreateMap<
                        Frontend.Models.BindingModels.Admin.UserEditBindingModel, 
                        Entities.User>();
                config
                    .CreateMap<
                        Entities.User, 
                        Frontend.Models.BindingModels.Admin.UserEditBindingModel>();
                config
                    .CreateMap<
                        Frontend.Models.BindingModels.Admin.UserDeleteBindingModel, 
                        Entities.User>();
                config
                    .CreateMap<
                        Entities.User,
                        Frontend.Models.BindingModels.Admin.UserDeleteBindingModel>();
            });
            //AutoMapper configuration end.
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Language"];
            if (cookie != null && cookie.Value != null)
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cookie.Value);
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cookie.Value);
            }
            else
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("EN");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("EN");
            }
        }   
    }
}
