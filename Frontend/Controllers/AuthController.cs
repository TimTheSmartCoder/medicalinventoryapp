﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Entities;
using Frontend.Models.ViewModels;
using Frontend.Models.ViewModels.BindinModels.AuthBindModels;
using ServiceGateway;
using ServiceGateway.Authentication;
using ServiceGateway.Exeption;
using ServiceGateway.Services;
using ServiceGateway.Util;
using AuthenticationManager = Frontend.Models.Authentication.AuthenticationManager;

namespace Frontend.Controllers
{
    public class AuthController : Controller
    {

        /// <summary>
        /// Redirects to the USerlogin action method
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public ActionResult Index(string url)
        {
            return RedirectToAction("UserLogin", url);
        }

        /// <summary>
        /// Opens the LoginView. If the user already is logget in it redirect
        /// the user to the correct action method.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public ActionResult UserLogin(string url)
        {
            RedirectViewModel model = new RedirectViewModel() {Url = url};

            if (AuthenticationManager.GetStorage(this.ControllerContext.HttpContext).IsValid())
            {
                DefaultServiceGateway gateway
                        = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));
                IUserService userService = gateway.GetService<User, IUserService>();
                    User user = userService.GetAuthenticatedUser();
                
                if (user.Admin)
                    return RedirectToAction("Index", "User");
                else
                    return Redirect("~/User/index");
            }
            return View("LoginView", model);
        }

        /// <summary>
        /// Authenticates the user if the user exists in authstorage.
        /// Redirects the user to the correct action method
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UserLogin(LoginBindingModel model)
        {

            if (model == null)
                return View("LoginView", new RedirectViewModel() {Url = model.Url});

            if (ModelState.IsValid)
            {
                try
                {
                    DefaultServiceGateway gateway
                        = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

                    gateway.Authenticate(new BearerAuthentication(model.Username, model.Password,
                        new RestApiUri("http://localhost:25311")));

                    IUserService userService = gateway.GetService<User, IUserService>();
                    User user = userService.GetAuthenticatedUser();

                    if (user.Admin)
                        return RedirectToAction("Index", "MedicineChest");

                    if (user.MedicalChest == null)
                    {
                        return RedirectToAction("ChestSelect");
                    }

                    if (model.Url != null)
                        return Redirect(model.Url);

                    return Redirect("~/User/index");
                }
                catch (AuthFailedExeption exception)
                {
                    ModelState.AddModelError(string.Empty, "The user could not be autherized.");

                    return View("LoginView", new RedirectViewModel() {Url = model.Url});
                }
            }

            ModelState.AddModelError(string.Empty, "You must fill out the filds correct.");
            return View("LoginView", new RedirectViewModel() {Url = model.Url});
        }
        
        /// <summary>
        /// returns the RegisterView
        /// </summary>
        /// <returns></returns>
        public ActionResult UserRegister()
        {
            return View("RegisterView");
        }

        /// <summary>
        /// Registers the user if rewurments are met.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UserRegister(RegisterUSerBindingModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (ModelState.IsValid)
            {
                try
                {

                    DefaultServiceGateway gateway
                        = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

                    IUserService userService = gateway.GetService<User, IUserService>();

                    userService.Create(new User
                    {
                        Email = model.Email,
                        Password = model.Password,
                        UserName = model.Username,
                    });

                    return View("LoginView", new RedirectViewModel()); 
                }
                catch (Exception exception)
                {
                    string e = exception.Message;
                    //ModelState.AddModelError(string.Empty, "The user could not be created.");

                    return View("RegisterView");
                }
            }
            ModelState.AddModelError(string.Empty, "The user could not be created.");
            return View("RegisterView");
        }

        /// <summary>
        /// Clears the current session and returns the user to the LoginView
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            this.ControllerContext.HttpContext.Session.Clear();

            return View("LoginView", new RedirectViewModel());
        }

        /// <summary>
        /// Returns the ChestSelectView with a list of chests
        /// </summary>
        /// <returns></returns>
        public ActionResult ChestSelect()
        {

            IDefaultService<MedicalChestInformation> medicalChestService = GetMedicalChestInformationSevice();

        IEnumerable <MedicalChestInformation> chests = medicalChestService.GetAll();

            return View("ChestSelectView", chests);
        }

        /// <summary>
        /// Connects the user with a chest and redirects to User index
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ChestSelect(int id)
        {
            IUserService userService = GetUserSevice();
            IDefaultService<MedicalChest> medicalChestService = GetMedicalChestSevice();

            User user = userService.GetAuthenticatedUser();

            medicalChestService.Create(new MedicalChest()
            {
                User = user,
                MedicalChestInformation = new MedicalChestInformation() {Id = id}
            });
            return Redirect("~/User/Index");
        }

        /// <summary>
        /// returns a medicalChestInformationService
        /// </summary>
        /// <returns>medicalChestInformationService</returns>
        private IDefaultService<MedicalChestInformation> GetMedicalChestInformationSevice()
        {
            DefaultServiceGateway serviceGateway
                = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

            return serviceGateway.GetService<MedicalChestInformation, IDefaultService<MedicalChestInformation>>();
        }

        /// <summary>
        /// gets a MedicalChestSevice
        /// </summary>
        /// <returns>MedicalChestSevice</returns>
        private IDefaultService<MedicalChest> GetMedicalChestSevice()
        {
            DefaultServiceGateway serviceGateway
                = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

            return serviceGateway.GetService<MedicalChest, IDefaultService<MedicalChest>>();
        }

        /// <summary>
        /// gets a UserSevice
        /// </summary>
        /// <returns>UserSevice</returns>
        private IUserService GetUserSevice()
        {
            DefaultServiceGateway serviceGateway
                = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

            return serviceGateway.GetService<User, IUserService>();
        }

    }
}