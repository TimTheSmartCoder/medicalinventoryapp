﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Frontend.Models.User;
using Entities;
using ServiceGateway.Services;
using ServiceGateway;
using Frontend.Models.Authentication;

namespace Frontend.Controllers
{
    public class UserController : Controller
    {
        [AuthorizeIsValid]
        // GET: User
        public ActionResult Index()
        {
            IDefaultService<MedicalChest> medicalChestService = this.GetMedicalChestService();
            IUserService userService = this.getUserInformationService();

            var user = userService.GetAuthenticatedUser();

            if (user.MedicalChest == null)
                return RedirectToAction("ChestSelect", "Auth");

            MedicineViewModel model = new MedicineViewModel();
            model.MedChest = medicalChestService.Get(userService.GetAuthenticatedUser().MedicalChest.Id);


            return View("~/Views/User/Index.cshtml", model);

        }

        [AuthorizeIsValid]
        public ActionResult Update()
        {
            IDefaultService<MedicalChest> medicalChestService = this.GetMedicalChestService();
            IUserService userService = this.getUserInformationService();

            MedicalChest medChest = medicalChestService.Get(userService.GetAuthenticatedUser().MedicalChest.Id);

            foreach (ItemGroup gruop in medChest.ItemGroups)
            {
                gruop.MedicalChest = null;
                gruop.ItemGroupInformation = null;
                
                foreach (Item item in gruop.Items)
                {
                    item.ItemInformation = null;
                    item.ItemGroup = null;

                    string totalstock = Request.Form[$"item-totalstock-{item.Id}"];

                    
                    DateTime expdateOne;
                    DateTime expdateTwo;
                    DateTime expdateThree;

                    DateTime? expdateOneNullable = null;
                    DateTime? expdateTwoNullable = null;
                    DateTime? expdateThreeNullable = null;

                    if (DateTime.TryParse(Request.Form[$"item-expdateOne-{item.Id}"], out expdateOne))
                        expdateOneNullable = expdateOne;
                      
                    if (DateTime.TryParse(Request.Form[$"item-expdateTwo-{item.Id}"], out expdateTwo))
                        expdateTwoNullable = expdateTwo;

                    if(DateTime.TryParse(Request.Form[$"item-expdateThree-{item.Id}"], out expdateThree))
                        expdateThreeNullable = expdateThree;
                    

                    item.TotalStock = totalstock;
                    item.ExpDateOne = expdateOneNullable;
                    item.ExpDateTwo = expdateTwoNullable;
                    item.ExpDateThree = expdateThreeNullable;
                }
                
            }

            medChest.MedicalChestInformation = null;

            medChest.ItemGroups = medChest.ItemGroups.FindAll(x => x.Id < 1313);
            medChest.MedicalChestInformation = null;
            medChest.User = null;
            medicalChestService.Update(medChest);

            MedicineViewModel model = new MedicineViewModel();
            model.MedChest = medicalChestService.Get(userService.GetAuthenticatedUser().MedicalChest.Id);

            return View("~/Views/User/Index.cshtml", model);
        }

        private IUserService getUserInformationService()
        {
            DefaultServiceGateway userService = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

            return userService.GetService<User, IUserService>();
        }

        private IDefaultService<MedicalChest> GetMedicalChestService()
        {
            DefaultServiceGateway medicalChest = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

            return medicalChest.GetService<MedicalChest, IDefaultService<MedicalChest>>();
        }

        private IDefaultService<ItemGroupInformation> GetItemGroupInformationService()
        {
            DefaultServiceGateway groupServiceGateway = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

            return groupServiceGateway.GetService<ItemGroupInformation, IDefaultService<ItemGroupInformation>>();
        }

        private IDefaultService<ItemInformation> GetItemInformationService()
        {
            DefaultServiceGateway serviceGateway = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

            return serviceGateway.GetService<ItemInformation, IDefaultService<ItemInformation>>();
        }
    }
}