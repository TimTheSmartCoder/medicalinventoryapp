﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Entities;
using Frontend.Models.Authentication;
using Frontend.Models.BindingModels.Admin;
using Frontend.Models.ViewModels.Admin.Medicin;
using ServiceGateway;
using ServiceGateway.Services;
using AuthenticationManager = Frontend.Models.Authentication.AuthenticationManager;

namespace Frontend.Controllers.Admin
{
    public class MedicinController : Controller
    {
        /// <summary>
        /// The index action.
        /// </summary>
        /// <returns></returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Index()
        {
            IDefaultService<ItemInformation> itemInformationService 
                = this.GetItemInformationService();

            MedicinViewModel model = new MedicinViewModel();
            model.ItemInformations = itemInformationService.GetAll();

            return View("~/Views/Admin/Medicin/MedicinView.cshtml", model);
        }

        /// <summary>
        /// Info action which will display information
        /// about the medicin.
        /// </summary>
        /// <param name="id">Id of medicin.</param>
        /// <returns></returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Info(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                IDefaultService<ItemInformation> itemInformationService
                = this.GetItemInformationService();

                MedicinInfoViewModel model = new MedicinInfoViewModel();
                model.ItemInformation = itemInformationService.Get(id.GetValueOrDefault());

                return View("~/Views/Admin/Medicin/MedicinInfoView.cshtml", model);
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Edit action method
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                IDefaultService<ItemInformation> itemInformationService
                = this.GetItemInformationService();

                ItemInformation itemInformation = itemInformationService.Get(id.GetValueOrDefault());
                ItemInformationLang lang = itemInformation.Information(Thread.CurrentThread.CurrentUICulture.Name);

                MedicinEditBindingModel model = new MedicinEditBindingModel();
                model.AtcCode = itemInformation.AtcCode;
                model.DanishAdministration = itemInformation.Information("da-DK").Administration;
                model.EnglishAdministration = itemInformation.Information("en").Administration;
                model.DanishMFag = itemInformation.Information("da-DK").Mfag;
                model.EnglishMFag = itemInformation.Information("en").Mfag;
                model.DanishName = itemInformation.Information("da-DK").Name;
                model.EnglishName = itemInformation.Information("en").Name;
                model.Id = id.GetValueOrDefault();

                return View("~/Views/Admin/Medicin/MedicinEditView.cshtml", model);
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Edit post action.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AuthorizeIsValid]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeIsAdmin]
        public ActionResult Edit(MedicinEditBindingModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                IDefaultService<ItemInformation> itemInformationService
                = this.GetItemInformationService();

                ItemInformation itemInformation = itemInformationService.Get(model.Id);
                ItemInformationLang daLang = itemInformation.Information("da-DK");
                ItemInformationLang enLang = itemInformation.Information("en");

                itemInformation.AtcCode = model.AtcCode;
                daLang.Administration = model.DanishAdministration;
                enLang.Administration = model.EnglishAdministration;
                daLang.Mfag = model.DanishMFag;
                enLang.Mfag = model.EnglishMFag;
                daLang.Name = model.DanishName;
                enLang.Name = model.EnglishName;

                itemInformationService.Update(itemInformation);

                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Create post action for medicin.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AuthorizeIsValid]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeIsAdmin]
        public ActionResult Create(MedicinCreateBindingModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                IDefaultService<ItemGroupInformation> itemGroupInformationService =
                    this.GetItemGroupInformationService();

                MedicinCreateBindingModel bindingModel = new MedicinCreateBindingModel();
                bindingModel.ItemGroupInformations = itemGroupInformationService.GetAll();

                if (!ModelState.IsValid)
                    return View("~/Views/Admin/Medicin/MedicinCreateView.cshtml", bindingModel);

                IDefaultService<ItemInformation> itemInformationService 
                    = this.GetItemInformationService();

                ItemInformationLang daLang = new ItemInformationLang() {Locale = "da-DK"};
                ItemInformationLang enLang = new ItemInformationLang() {Locale = "en"};
                ItemInformation itemInformation = new ItemInformation();
                itemInformation.ItemInformationLangs = new List<ItemInformationLang>()
                {
                    daLang,
                    enLang
                };

                itemInformation.AtcCode = model.AtcCode;
                daLang.Administration = model.DanishAdministration;
                enLang.Administration = model.EnglishAdministration;
                daLang.Mfag = model.DanishMFag;
                enLang.Mfag = model.EnglishMFag;
                daLang.Name = model.DanishName;
                enLang.Name = model.EnglishName;

                itemInformation.ItemGroupInformation = new ItemGroupInformation() { Id = model.ItemGroup};

                itemInformationService.Create(itemInformation);

                return View("~/Views/Admin/Medicin/MedicinCreateView.cshtml", bindingModel);
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        /// <summary>
        /// Create action for allowing the
        /// user to create an medicin.
        /// </summary>
        /// <returns></returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Create()
        {
            try
            {
                IDefaultService<ItemGroupInformation> itemGroupInformationService =
                    this.GetItemGroupInformationService();

                MedicinCreateBindingModel bindingModel = new MedicinCreateBindingModel();
                bindingModel.ItemGroupInformations = itemGroupInformationService.GetAll();

                return View("~/Views/Admin/Medicin/MedicinCreateView.cshtml", bindingModel);
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Delete action which allows the user to
        /// delete an medicin.
        /// </summary>
        /// <param name="id">Id of the medicin to delete.</param>
        /// <returns></returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                IDefaultService<ItemInformation> itemInformationService
                    = this.GetItemInformationService();

                ItemInformation itemInformation = itemInformationService.Get(id.GetValueOrDefault());

                MedicinDeleteBindingModel model = new MedicinDeleteBindingModel();
                model.Id = itemInformation.Id;
                model.Name = itemInformation.Information(Thread.CurrentThread.CurrentUICulture.Name).Name;

                return View("~/Views/Admin/Medicin/MedicinDeleteView.cshtml", model);
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Delete post action for deleting a medicin.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Delete(MedicinDeleteBindingModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return View("~/Views/Admin/Medicin/MedicinDeleteView.cshtml", model);

            try
            {
                IDefaultService<ItemInformation> itemInformationService
                    = this.GetItemInformationService();

                itemInformationService.Delete(new ItemInformation() {Id = model.Id});

                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Gets ItemInformationService.
        /// </summary>
        /// <returns></returns>
        private IDefaultService<ItemInformation> GetItemInformationService()
        {
            DefaultServiceGateway serviceGateway
                = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

            return serviceGateway.GetService<ItemInformation, IDefaultService<ItemInformation>>();
        }

        /// <summary>
        /// Gets ItemGroupInformationService.
        /// </summary>
        /// <returns></returns>
        private IDefaultService<ItemGroupInformation> GetItemGroupInformationService()
        {
            DefaultServiceGateway serviceGateway
                = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

            return serviceGateway.GetService<ItemGroupInformation, IDefaultService<ItemGroupInformation>>();
        }
    }
}