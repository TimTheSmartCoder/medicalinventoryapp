﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Entities;
using Frontend.Models.Authentication;
using Frontend.Models.BindingModels.Admin;
using Frontend.Models.ViewModels.Admin.Medicin;
using Frontend.Models.ViewModels.Admin.MedicineGroup;
using ServiceGateway;
using ServiceGateway.Services;
using AuthenticationManager = Frontend.Models.Authentication.AuthenticationManager;

namespace Frontend.Controllers.Admin
{
    public class MedicineGroupController : Controller
    {
        
        // GET: MedicineGroup
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Index()
        {
            IDefaultService<ItemGroupInformation> itemGroupInformationService = this.GetItemGroupInformationService();
            MedicineGroupViewModel model = new MedicineGroupViewModel();
            model.ItemGroupInformations = itemGroupInformationService.GetAll();

            return View("~/Views/Admin/MedicineGroup/MedicineGroupView.cshtml", model);
        }

        /// <summary>
        /// Gets MedicalChestInformationService
        /// </summary>
        /// <returns>MedicalChestInformationService</returns>
        private IDefaultService<MedicalChestInformation> GetMedicalChestInformationService()
        {
            DefaultServiceGateway serviceGateway
                = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

            return serviceGateway.GetService<MedicalChestInformation, IDefaultService<MedicalChestInformation>>();
        }

        /// <summary>
        /// Gets MedicalChestService
        /// </summary>
        /// <returns>MedicalChestService</returns>
        private IDefaultService<MedicalChest> GetMedicalChestService()
        {
            DefaultServiceGateway serviceGateway
                = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

            return serviceGateway.GetService<MedicalChest, IDefaultService<MedicalChest>>();
        }

        /// <summary>
        /// Gets ItemGroupInformationService
        /// </summary>
        /// <returns>ItemGroupInformationService</returns>
        private IDefaultService<ItemGroupInformation> GetItemGroupInformationService()
        {
            DefaultServiceGateway serviceGateway
                = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

            return serviceGateway.GetService<ItemGroupInformation, IDefaultService<ItemGroupInformation>>();
        }

        /// <summary>
        /// Returns the MedicineGroupInfoView with a MedicineGroupInfoViewModel
        /// containing a ItemGroupInformation 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>MedicineGroupInfoView</returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Info(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                IDefaultService<ItemGroupInformation> itemGroupInformationService
                = this.GetItemGroupInformationService();

                MedicineGroupInfoViewModel model = new MedicineGroupInfoViewModel();
                model.ItemGroupInformation = itemGroupInformationService.Get(id.GetValueOrDefault());

                return View("~/Views/Admin/MedicineGroup/MedicineGroupInfoView.cshtml", model);
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Returns the MedicineGroupEditView
        /// </summary>
        /// <param name="id"></param>
        /// <returns>MedicineGroupEditView</returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                IDefaultService<ItemGroupInformation> itemGroupInformationService = GetItemGroupInformationService();

                ItemGroupInformation itemGroupInformation = itemGroupInformationService.Get(id.GetValueOrDefault());

                MedicineGroupEditBindingModel model = new MedicineGroupEditBindingModel();
                model.DanishName = itemGroupInformation.Information("da-DK").Name;
                model.EnglishName = itemGroupInformation.Information("en").Name;
                model.Id = id.GetValueOrDefault();


                return View("~/Views/Admin/MedicineGroup/MedicineGroupEditView.cshtml", model);
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Edits the ItemGroupInformation an return to the index 
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Index</returns>
        [AuthorizeIsValid]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeIsAdmin]
        public ActionResult Edit(MedicineGroupEditBindingModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                IDefaultService<ItemGroupInformation> itemGroupInformationService = GetItemGroupInformationService();

                ItemGroupInformation itemGroupInformation = itemGroupInformationService.Get(model.Id);
                ItemGroupInformationLang daLang = itemGroupInformation.Information("da-DK");
                ItemGroupInformationLang enLang = itemGroupInformation.Information("en");
                
                daLang.Name = model.DanishName;
                enLang.Name = model.EnglishName;

                itemGroupInformationService.Update(itemGroupInformation);

                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Creates a MedicineGroup based on the input in the MedicineGroupCreateBindingModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns>MedicineGroupCreateView</returns>
        [AuthorizeIsValid]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeIsAdmin]
        public ActionResult Create(MedicineGroupCreateBindingModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                //IDefaultService<MedicalChestInformation> medicalChestInformationService =
                //    this.GetMedicalChestInformationService();

                MedicineGroupCreateBindingModel bindingModel = new MedicineGroupCreateBindingModel();
                //bindingModel.MedicalChestInformations = medicalChestInformationService.GetAll();
                IDefaultService<MedicalChestInformation> service = this.GetMedicalChestInformationService();
                bindingModel.MedicalChestInformations = service.GetAll();


                if (!ModelState.IsValid)
                    return View("~/Views/Admin/MedicineGroup/MedicineGroupCreateView.cshtml", bindingModel);

                IDefaultService<ItemGroupInformation> itemGroupInformationService
                    = this.GetItemGroupInformationService();

                ItemGroupInformationLang daLang = new ItemGroupInformationLang() { Locale = "da-DK" };
                ItemGroupInformationLang enLang = new ItemGroupInformationLang() { Locale = "en" };
                ItemGroupInformation itemGroupInformation = new ItemGroupInformation();
                itemGroupInformation.ItemGroupInformationLangs = new List<ItemGroupInformationLang>()
                {
                    daLang,
                    enLang
                };

                daLang.Name = model.DanishName;
                enLang.Name = model.EnglishName;

                //itemGroupInformation.MedicalChestInformation = new MedicalChestInformation() { Id = model.MedicalChestInformation };
                itemGroupInformation.MedicalChestInformation = new MedicalChestInformation() { Id = model.MedicalChestInformation };
                
                itemGroupInformationService.Create(itemGroupInformation);

                return View("~/Views/Admin/MedicineGroup/MedicineGroupCreateView.cshtml", bindingModel);
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// returns the MedicineGroupCreateView with a MedicineGroupCreateBindingModel
        /// containing a list of MedicalChestInformations
        /// </summary>
        /// <returns>MedicineGroupCreateView</returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Create()
        {
            try
            {
                IDefaultService<ItemGroupInformation> itemGroupInformationService =
                    this.GetItemGroupInformationService();

                MedicineGroupCreateBindingModel model = new MedicineGroupCreateBindingModel();
                model.MedicalChestInformations = GetMedicalChestInformationService().GetAll();
                return View("~/Views/Admin/MedicineGroup/MedicineGroupCreateView.cshtml", model);
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// returns the MedicineGroupDeleteView with the chosen ItemGroupInformation to delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns>MedicineGroupDeleteView</returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                IDefaultService<ItemGroupInformation> itemGroupInformationService
                    = this.GetItemGroupInformationService();

                ItemGroupInformation itemGroupInformation = itemGroupInformationService.Get(id.GetValueOrDefault());

                MedicineGroupDeleteBindingModel model = new MedicineGroupDeleteBindingModel();
                model.Id = itemGroupInformation.Id;
                model.Name = itemGroupInformation.Information(Thread.CurrentThread.CurrentUICulture.Name).Name;

                return View("~/Views/Admin/MedicineGroup/MedicineGroupDeleteView.cshtml", model);
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Posts the information for the ItemGroupInformation to be deleted
        /// Returns the MedicinDeleteView if mdoelstate not valid
        /// Returns the Index if succesfull 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AuthorizeIsAdmin]
        public ActionResult Delete(MedicinDeleteBindingModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return View("~/Views/Admin/Medicin/MedicinDeleteView.cshtml", model);

            try
            {
                IDefaultService<ItemGroupInformation> itemGroupInformationService
                    = this.GetItemGroupInformationService();

                itemGroupInformationService.Delete(new ItemGroupInformation() { Id = model.Id });

                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

    }
}