﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Entities;
using Frontend.Models.Authentication;
using Frontend.Models.BindingModels.Admin;
using Frontend.Models.ViewModels.Admin.User;
using ServiceGateway;
using ServiceGateway.Services;
using AuthenticationManager = Frontend.Models.Authentication.AuthenticationManager;

namespace Frontend.Controllers.Admin
{
    public class UserController : Controller
    {
        /// <summary>
        /// Index of the UserController.
        /// </summary>
        /// <returns></returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Index()
        {
            return RedirectToAction("Pagination", new {pageIndex = 1, pageSize = 10});
        }

        /// <summary>
        /// Returns the userview with the the given pageindex and page size as a UserViewModel
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>Userview</returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Pagination(int? pageIndex, int? pageSize)
        {
            IUserService userService = this.GetUserService();
            UserViewModel userViewModel = new UserViewModel();

            try
            {
                //Get the number of users in the backend.
                int count = userService.Count();

                userViewModel.NumberOfPages = (count < pageSize.GetValueOrDefault()) ? 1 : count / pageSize.GetValueOrDefault();
                userViewModel.CurrentPage = pageIndex.GetValueOrDefault();
                userViewModel.Users = userService.Pagination(userViewModel.CurrentPage, pageSize.GetValueOrDefault());

                return View("~/Views/Admin/User/UserView.cshtml", userViewModel);
            }
            catch (Exception exception)
            {
                return View("~/Views/Admin/User/UserView.cshtml", userViewModel);
            }
        }

        /// <summary>
        /// Returns the Info view to the user, with the given
        /// id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Info(int? id)
        {
            if (id == null)
                return HttpNotFound();

            IUserService userService = this.GetUserService();
            UserInfoViewModel infoViewModel = new UserInfoViewModel();

            try
            {
                infoViewModel.User = userService.Get(id.GetValueOrDefault());
                return View("~/Views/Admin/User/UserInfoView.cshtml", infoViewModel);
            }
            catch (Exception exception)
            {
                return HttpNotFound();    
            }
        }

        /// <summary>
        /// Returns the UserEditView with a 
        /// bindingModel containg the username and id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>UserEditView</returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return HttpNotFound();

            IUserService userService = GetUserService();

            try
            {
                var bindingModel
                    = Mapper.Map<UserEditBindingModel>(userService.Get(id.GetValueOrDefault()));
                
                return View("~/Views/Admin/User/UserEditView.cshtml", bindingModel);
            }
            catch (Exception exception)
            {
                return HttpNotFound();
            }
        }

        /// <summary>
        /// Posts the information for the user to edit.
        /// returns a UserEditView
        /// </summary>
        /// <param name="model"></param>
        /// <returns>UserEditView</returns>
        [AuthorizeIsValid]
        [HttpPost]
        [AuthorizeIsAdmin]
        public ActionResult Edit(UserEditBindingModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            
            try
            {
                var userService = this.GetUserService();

                model.UserName = userService.Get(model.Id).UserName;

                if (!ModelState.IsValid)
                    return View("~/Views/Admin/User/UserEditView.cshtml", model);

                User user = Mapper.Map<User>(model);
                
                user.Password = userService.Get(user.Id).Password;
                userService.Update(user);

                return View("~/Views/Admin/User/UserEditView.cshtml", model);
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// returns a UserDeleteView with a UserDeleteBindingModel
        /// </summary>
        /// <param name="id"></param>
        /// <returns>UserDeleteView</returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return HttpNotFound();

            try
            {
                var user = this.GetUserService().Get(id.GetValueOrDefault());

                UserDeleteBindingModel model = Mapper.Map<UserDeleteBindingModel>(user);

                return View("~/Views/Admin/User/UserDeleteView.cshtml", model);
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Posts the information for the user to delete
        /// returns the index 
        /// </summary>
        /// <param name="model"></param>
        /// <returns>index</returns>
        [AuthorizeIsValid]
        [HttpPost]
        [AuthorizeIsAdmin]
        public ActionResult Delete(UserDeleteBindingModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                var userService = this.GetUserService();
                var user = userService.Get(model.Id);
                userService.Delete(user);
               
                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Gets the UserService
        /// </summary>
        /// <returns>UserService</returns>
        private IUserService GetUserService()
        {
            DefaultServiceGateway serviceGateway 
                = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));
            return serviceGateway.GetService<User, IUserService>();
        }
    }
}