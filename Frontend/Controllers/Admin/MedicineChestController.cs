﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Entities;
using Frontend.Models.Authentication;
using Frontend.Models.BindingModels.Admin;
using Frontend.Models.ViewModels.Admin.MedicineChest;
using ServiceGateway;
using ServiceGateway.Services;
using AuthenticationManager = Frontend.Models.Authentication.AuthenticationManager;

namespace Frontend.Controllers.Admin
{
    public class MedicineChestController : Controller
    {
        /// <summary>
        /// The index action for medicinchest.
        /// </summary>
        /// <returns></returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Index()
        {
            MedicineChestViewModel model = new MedicineChestViewModel();
            model.MedicalChestInformations = this.GetMedicalChestInformationService().GetAll();

            return View("~/Views/Admin/MedicineChest/MedicineChestView.cshtml", model);
        }

        /// <summary>
        /// The create action method for create a medicin chest.
        /// </summary>
        /// <returns></returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Create()
        {
            MedicineChestCreateBindingModel model = new MedicineChestCreateBindingModel();

            return View("~/Views/Admin/MedicineChest/MedicineChestCreateView.cshtml", model);
        }

        /// <summary>
        /// Create post action for medicin chest.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeIsAdmin]
        public ActionResult Create(MedicineChestCreateBindingModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return View("~/Views/Admin/MedicineChest/MedicineChestCreateView.cshtml", model);

            IDefaultService<MedicalChestInformation> service = this.GetMedicalChestInformationService();
            service.Create(new MedicalChestInformation()
            {
                MedicalChestInformationLangs = new List<MedicalChestInformationLang>()
                {
                    new MedicalChestInformationLang() { Locale = "da-DK", Name = model.DanishName},
                    new MedicalChestInformationLang() { Locale = "en",  Name = model.EnglishName } 
                }
            });

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Gets the MedicalChestInformationService.
        /// </summary>
        /// <returns></returns>
        private IDefaultService<MedicalChestInformation> GetMedicalChestInformationService()
        {
            DefaultServiceGateway serviceGateway 
                = new DefaultServiceGateway(AuthenticationManager.GetStorage(this.ControllerContext.HttpContext));

            return serviceGateway.GetService<MedicalChestInformation, IDefaultService<MedicalChestInformation>>();
        }

        /// <summary>
        /// Edit action method.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                IDefaultService<MedicalChestInformation> medicalChestInformationService = GetMedicalChestInformationService();

                MedicalChestInformation medicalChestInformation = medicalChestInformationService.Get(id.GetValueOrDefault());

                MedicineChestEditBindingModel model = new MedicineChestEditBindingModel();
                model.DanishName = medicalChestInformation.Information("da-DK").Name;
                model.EnglishName = medicalChestInformation.Information("en").Name;
                model.Id = medicalChestInformation.Id;
                
                return View("~/Views/Admin/MedicineChest/MedicineChestEditView.cshtml", model);
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Edit post method.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AuthorizeIsValid]
        [HttpPost]
        [AuthorizeIsAdmin]
        public ActionResult Edit(MedicineChestEditBindingModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                IDefaultService<MedicalChestInformation> medicalChestInformationService = GetMedicalChestInformationService();

                MedicalChestInformation medicalChestInformation = medicalChestInformationService.Get(model.Id);
                MedicalChestInformationLang daLang = medicalChestInformation.Information("da-DK");
                MedicalChestInformationLang enLang = medicalChestInformation.Information("en");

                daLang.Name = model.DanishName;
                enLang.Name = model.EnglishName;

                medicalChestInformationService.Update(medicalChestInformation);

                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Delete action method.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AuthorizeIsValid]
        [HttpPost]
        [AuthorizeIsAdmin]
        public ActionResult Delete(MedicineChestDeleteBindingModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (!ModelState.IsValid)
                return View("~/Views/Admin/MedicineChest/MedicineChestDeleteView.cshtml", model);

            try
            {
                IDefaultService<MedicalChestInformation> medicalChestInformationService
                    = this.GetMedicalChestInformationService();

                medicalChestInformationService.Delete(new MedicalChestInformation() { Id = model.Id });

                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Delete action method.
        /// </summary>
        /// <param name="id">Id of the medicin chest ot delete.</param>
        /// <returns></returns>
        [AuthorizeIsValid]
        [AuthorizeIsAdmin]
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            try
            {
                IDefaultService<MedicalChestInformation> medicalChestInformationService
                    = this.GetMedicalChestInformationService();

                MedicalChestInformation medicalChestInformation = medicalChestInformationService.Get(id.GetValueOrDefault());

                MedicineChestDeleteBindingModel model = new MedicineChestDeleteBindingModel();
                model.Id = medicalChestInformation.Id;
                model.Name = medicalChestInformation.Information(Thread.CurrentThread.CurrentUICulture.Name).Name;

                return View("~/Views/Admin/MedicineChest/MedicineChestDeleteView.cshtml", model);
            }
            catch (Exception exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
    }
}