﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Action = Antlr.Runtime.Misc.Action;

namespace Frontend.Controllers
{
    public class LanguageController : Controller
    {
        /// <summary>
        /// isnt used and dosent return anything
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Sets the currentCulture info to the languageTag.
        /// Creates a new cookie containing the languageTag. 
        /// </summary>
        /// <param name="LanguageTag"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Change(string LanguageTag, string returnUrl)
        {
            if (LanguageTag != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(LanguageTag);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(LanguageTag);                
            }           
            HttpCookie cookie = new  HttpCookie("Language");
            cookie.Value = LanguageTag;
            Response.Cookies.Add(cookie);

            return Redirect(returnUrl);
        }
    }
}