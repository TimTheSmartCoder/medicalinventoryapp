﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class MedicalChestInformationLang : AbstractEntity
    {
        /**
         * Fluent api binding properties.
         */
        public List<MedicalChestInformation> MedicalChestInformations { get; set; }

        public string Locale { get; set; }
        public string Name { get; set; }
    }
}
