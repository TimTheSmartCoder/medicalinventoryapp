﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class ItemGroupInformationLang : AbstractEntity
    {
        /**
         * Fluent api binding properties.
         */
        public List<ItemGroupInformation> ItemGroupInformations { get; set; }

        public string Name { get; set; }
        public string Locale { get; set; }
    }
}
