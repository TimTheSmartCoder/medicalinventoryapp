﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class ItemGroup : AbstractEntity
    {
        /**
         * Fluent api binding properties.
         */
        public MedicalChest MedicalChest { get; set; }
        public List<Item> Items { get; set; }
        public ItemGroupInformation ItemGroupInformation { get; set; }        
    }
}
