﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class ItemInformationLang : AbstractEntity
    {
        /**
         * Fluent api binding properties.
         */
        public List<ItemInformation> ItemInformations { get; set; }
        public string Name { get; set; }
        public string ItemAmount { get; set; }
        public string Administration { get; set; }
        public string Mfag { get; set; }
        public string Unit { get; set; }
        public string Locale { get; set; }
        //public string LabelName { get; set; }
        //public string LabelText { get; set; }
        //public string LabelForm { get; set; }
        //public string LabelGroupNr { get; set; }
    }
}
