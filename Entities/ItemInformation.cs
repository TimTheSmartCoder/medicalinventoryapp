﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class ItemInformation : AbstractEntity
    {/**
         * Fluent api binding properties.
         */
        public List<ItemInformationLang> ItemInformationLangs { get; set; }
        public List<Item> Items { get; set; }
        public ItemGroupInformation ItemGroupInformation { get; set; }

        //Get the language information.
        public ItemInformationLang Information(string locale)
        {
            foreach (var itemInformationLang in this.ItemInformationLangs)
            {
                if (itemInformationLang.Locale.ToLower().Equals(locale.ToLower()))
                    return itemInformationLang;
            }

            return null;
        }

        //private string _groupNumber;
        //public string GroupNumber
        //{
        //    get { return _groupNumber; }
        //    set
        //    {
        //        if (value.EndsWith("."))
        //        {
        //            value = value.Substring(0, 1);
        //        }
        //        _groupNumber = value;
        //    }
        //}

        public string GroupNumber => (this.ItemGroupInformation == null) ? "" : $"{this.ItemGroupInformation.Id}.{this.Id}";

        public string AtcCode { get; set; }
        public string Strength { get; set; }
    }
}
