﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class AbstractEntity
    {
        public const int DefaultId = 0;

        public int Id { get; set; }
    }
}
