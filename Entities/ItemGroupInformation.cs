﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class ItemGroupInformation : AbstractEntity
    {
        /**
         * Fluent api binding properties.
         */
        public List<ItemGroup> ItemGroups { get; set; }
        public List<ItemInformation> ItemInformations { get; set; }
        public List<ItemGroupInformationLang> ItemGroupInformationLangs { get; set; }
        public MedicalChestInformation MedicalChestInformation { get; set; }
        public int MedicalChestInformationId { get; set; }

        public string GroupNumber {
            get
            {
                if (this._groupNumber == null)
                    return this.Id.ToString();
                else
                    return this._groupNumber;
            }
            set { this._groupNumber = value; }
        }
        public string Name { get; set; }

        private string _groupNumber;

        //Get the language information.
        public ItemGroupInformationLang Information(string locale)
        {
            foreach (var itemGroupInformationLang in this.ItemGroupInformationLangs)
            {
                if (itemGroupInformationLang.Locale.ToLower().Equals(locale.ToLower()))
                    return itemGroupInformationLang;
            }

            return null;
        }

        public string GetTitle()
        {
            string locale = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
            return ItemGroupInformationLangs.FirstOrDefault(y => y.Locale.ToLower() == locale.ToLower()).Name;
        }
    }
}
