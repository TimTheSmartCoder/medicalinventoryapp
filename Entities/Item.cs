﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Item : AbstractEntity
    {
        /**
         * Fluent api binding properties.
         */
        public ItemGroup ItemGroup { get; set; }
        public ItemInformation ItemInformation { get; set; }

        public string TotalStock { get; set; }
        public DateTime? ExpDateOne { get; set; }
        public DateTime? ExpDateTwo { get; set; }
        public DateTime? ExpDateThree { get; set; }
    }
}
