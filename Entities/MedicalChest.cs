﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class MedicalChest : AbstractEntity
    {
        /**
         * Fluent api binding properties.
         */
        public User User { get; set; }
        public MedicalChestInformation MedicalChestInformation { get; set; }
        public List<ItemGroup> ItemGroups { get; set; }
        
    }
}
