﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class MedicalChestInformation : AbstractEntity
    {
        /**
         * Fluent api binding properties.
         */
        public List<MedicalChestInformationLang> MedicalChestInformationLangs { get; set; }
        public List<MedicalChest> MedicalChests { get; set; }
        public List<ItemGroupInformation> ItemGroupsInformations { get; set; }

        //Get the language information.
        public MedicalChestInformationLang Information(string locale)
        {
            foreach (var chestInformationLang in this.MedicalChestInformationLangs)
            {
                if (chestInformationLang.Locale.ToLower().Equals(locale.ToLower()))
                    return chestInformationLang;
            }
            return null;
        }
    }
}
