﻿using System;
using Backend;
using Backend.Exceptions;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.Backend
{
    [TestClass]
    public class ItemInformationRepositoryTest
    {
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            ItemInformationRepository = RepositoryFacade.GetItemInformationManager();
        }

        private static IRepository<ItemInformation> ItemInformationRepository { get; set; }

        [TestMethod]
        public void GetIfNotExist()
        {
            try
            {
                ItemInformationRepository.Get(10000);
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void CreateIfNull()
        {
            try
            {
                ItemInformationRepository.Create(null);
                Assert.Fail();
            }
            catch (ArgumentNullException exception)
            {
                return;
            }
        }


        [TestMethod]
        public void UpdateIfNotExist()
        {
            try
            {
                ItemInformation itemInformation = new ItemInformation() { Id = 10000 };
                ItemInformationRepository.Update(itemInformation);
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void UpdateIfNull()
        {
            try
            {
                ItemInformationRepository.Update(null);
                Assert.Fail();
            }
            catch (ArgumentNullException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void DeleteIfNotExists()
        {
            try
            {
                ItemInformationRepository.Delete(new ItemInformation() { Id = 10000 });
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }
    }
}
