﻿using System;
using Backend;
using Backend.Contexts;
using Backend.Exceptions;
using Backend.Repositories;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.Backend
{
    [TestClass]
    public class UserRepositoryTest
    {
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            UserRepository = RepositoryFacade.GetUserManager();
        }

        public static IUserRepository UserRepository { get; set; }

        [TestMethod]
        public void GetIfNotExist()
        {
            try
            {
                UserRepository.Get(10000);
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void GetIfExists()
        {
            try
            {
                User user = UserRepository.Create(new User()
                {
                    UserName = "username",
                    Email = "mail@mail.dk",
                    Password = "123456789"
                });

                UserRepository.Get(user.Id);
                Assert.Fail();
            }
            catch (Exception exception)
            {
                return;
            }
        }

        [TestMethod]
        public void CreateIfNull()
        {
            try
            {
                UserRepository.Create(null);
                Assert.Fail();
            }
            catch (ArgumentNullException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void Create()
        {
            try
            {
                User user = UserRepository.Create(new User()
                {
                    Email = "mail@mail.dk",
                    Password = "123456",
                    UserName = "test"
                });

                UserRepository.Create(user);

                Assert.Fail();
            }
            catch (AlreadyExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void CreateAlreadytExist()
        {
            try
            {
                User user = UserRepository.Create(new User()
                {
                    Id = 10000,
                    Email = "mail@mail.dk",
                    Password = "123456",
                    UserName = "test"
                });

                UserRepository.Create(user);

                Assert.Fail();
            }
            catch (AlreadyExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void UpdateIfNull()
        {
            try
            {
                UserRepository.Update(null);
                Assert.Fail();
            }
            catch (ArgumentNullException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void UpdateIfNotExist()
        {
            try
            {
                User user = new User() {Id = 10000};
                UserRepository.Update(user);

                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void DeleteIfExists()
        {
            try
            {
                User user = UserRepository.Create(new User()
                {
                    Email = "mail@mail.dk",
                    Password = "123456",
                    UserName = "test"
                });

                UserRepository.Delete(user);
                Assert.Fail();
            }
            catch (Exception exception)
            {
                return;
            }
        }

        [TestMethod]
        public void DeleteIfNotExists()
        {
            try
            {
                UserRepository.Delete(new User() {Id = 10000});
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void GetFromUserNameIfDoNotExist()
        {
            try
            {
                UserRepository.GetFromUserName("123456789");
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void GetFromEmailIfDoNotExist()
        {
            try
            {
                UserRepository.GetFromEmail("email@mail.dk");
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }
    }
}
