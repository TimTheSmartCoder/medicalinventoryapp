﻿using System;
using Backend;
using Backend.Exceptions;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.Backend
{
    [TestClass]
    public class ItemGroupRepositoryTest
    {
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            ItemGroupRepository = RepositoryFacade.GetItemGroupRepository();
        }
        private static IRepository<ItemGroup> ItemGroupRepository { get; set; }

        [TestMethod]
        public void GetIfNotExist()
        {
            try
            {
                ItemGroupRepository.Get(10000);
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void CreateIfNull()
        {
            try
            {
                ItemGroupRepository.Create(null);
                Assert.Fail();
            }
            catch (ArgumentNullException exception)
            {
                return;
            }
        }


        [TestMethod]
        public void UpdateIfNotExist()
        {
            try
            {
                ItemGroup itemGroup = new ItemGroup() { Id = 10000 };
                ItemGroupRepository.Update(itemGroup);
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void UpdateIfNull()
        {
            try
            {
                ItemGroupRepository.Update(null);
                Assert.Fail();
            }
            catch (ArgumentNullException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void DeleteIfNotExists()
        {
            try
            {
                ItemGroupRepository.Delete(new ItemGroup() { Id = 10000 });
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }
    }
}
