﻿using System;
using Backend;
using Backend.Exceptions;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.Backend
{
    [TestClass]
    public class ItemGroupInformationRepositoryTest
    {
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            ItemGroupInformationRepository = RepositoryFacade.GetItemGroupInformationManager();
        }
        private static IRepository<ItemGroupInformation> ItemGroupInformationRepository { get; set; }

        [TestMethod]
        public void GetIfNotExist()
        {
            try
            {
                ItemGroupInformationRepository.Get(10000);
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void CreateIfNull()
        {
            try
            {
                ItemGroupInformationRepository.Create(null);
                Assert.Fail();
            }
            catch (ArgumentNullException exception)
            {
                return;
            }
        }


        [TestMethod]
        public void UpdateIfNotExist()
        {
            try
            {
                ItemGroupInformation itemGroupInformation = new ItemGroupInformation() { Id = 10000 };
                ItemGroupInformationRepository.Update(itemGroupInformation);
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void UpdateIfNull()
        {
            try
            {
                ItemGroupInformationRepository.Update(null);
                Assert.Fail();
            }
            catch (ArgumentNullException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void DeleteIfNotExists()
        {
            try
            {
                ItemGroupInformationRepository.Delete(new ItemGroupInformation() { Id = 10000 });
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }
    }
}
