﻿using System;
using Backend;
using Backend.Exceptions;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.Backend
{
    [TestClass]
    public class ItemRepositoryTest
    {
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            ItemRepository = RepositoryFacade.GetItemManager();
        }

        private static IRepository<Item> ItemRepository { get; set; }

        [TestMethod]
        public void GetIfNotExist()
        {
            try
            {
                ItemRepository.Get(10000);
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void CreateIfNull()
        {
            try
            {
                ItemRepository.Create(null);
                Assert.Fail();
            }
            catch (ArgumentNullException exception)
            {
                return;
            }
        }


        [TestMethod]
        public void UpdateIfNotExist()
        {
            try
            {
                Item item = new Item() { Id = 10000 };
                ItemRepository.Update(item);
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void UpdateIfNull()
        {
            try
            {
                ItemRepository.Update(null);
                Assert.Fail();
            }
            catch (ArgumentNullException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void DeleteIfNotExists()
        {
            try
            {
                ItemRepository.Delete(new Item() { Id = 10000 });
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }
    }
}
