﻿using System;
using Backend;
using Backend.Exceptions;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.Backend
{
    [TestClass]
    public class MedicalChestRepositoryTest
    {
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            MedicalChestRepository = RepositoryFacade.GetMedicalChestManager();
        }

        private static IRepository<MedicalChest> MedicalChestRepository { get; set; }

        [TestMethod]
        public void GetIfNotExist()
        {
            try
            {
                MedicalChestRepository.Get(10000);
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void CreateIfNull()
        {
            try
            {
                MedicalChestRepository.Create(null);
                Assert.Fail();
            }
            catch (ArgumentNullException exception)
            {
                return;
            }
        }


        [TestMethod]
        public void UpdateIfNotExist()
        {
            try
            {
                MedicalChest medicalChest = new MedicalChest() {Id = 10000};
                MedicalChestRepository.Update(medicalChest);
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void UpdateIfNull()
        {
            try
            {
                MedicalChestRepository.Update(null);
                Assert.Fail();
            }
            catch (ArgumentNullException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void DeleteIfNotExists()
        {
            try
            {
                MedicalChestRepository.Delete(new MedicalChest() {Id = 10000});
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }
    }
}
