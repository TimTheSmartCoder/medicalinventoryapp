﻿using System;
using Backend;
using Backend.Exceptions;
using Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.Backend
{
    [TestClass]
    public class MedicalChestInformationTest
    {
        [ClassInitialize]
        public static void Initalize(TestContext context)
        {
            Repository = RepositoryFacade.GetMedicalChestInformationManager();
        }

        private static IRepository<MedicalChestInformation> Repository { get; set; }

        [TestMethod]
        public void GetIfNotExists()
        {
            try
            {
                Repository.Get(10000);
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void CreateIfNull()
        {
            try
            {
                Repository.Create(null);
                Assert.Fail();
            }
            catch (ArgumentNullException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void UpdateIfNull()
        {
            try
            {
                Repository.Update(null);
                Assert.Fail();
            }
            catch (ArgumentNullException exception)
            {
                return;
            }
        }

        [TestMethod]
        public void UpdateIfDoNoExists()
        {
            try
            {
                Repository.Update(new MedicalChestInformation() {Id = 10000});
                Assert.Fail();
            }
            catch (DoNotExistException exception)
            {
                return;
            }
        }
    }
}
