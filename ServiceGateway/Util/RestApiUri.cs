﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceGateway.Util
{
    /// <summary>
    /// RestApiUri is for handling an url for services.
    /// </summary>
    public class RestApiUri
    {
        public RestApiUri(string uri)
        {
            //Uri to use to convert string into different parts.
            Uri conversion = new Uri(uri);

            this.Host = $"{conversion.Scheme}://{conversion.Host}:{conversion.Port}";
            this.AbsolutePath = conversion.AbsolutePath;
        }

        public string Host { get; }
        public string AbsolutePath { get; }
    }
}

