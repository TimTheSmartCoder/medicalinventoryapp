﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using ServiceGateway.Authentication;
using ServiceGateway.Exeption;
using ServiceGateway.Services;
using ServiceGateway.Util;

namespace ServiceGateway
{
    /// <summary>
    /// DefaultServiceGateway is the service gateway which is used
    /// for contact different services.
    /// </summary>
    public class DefaultServiceGateway
    {
        /// <summary>
        /// Constructs an DefaultServiceGateway which uses the given
        /// AuthStorage.
        /// </summary>
        /// <param name="authStorage"></param>
        public DefaultServiceGateway(IAuthStorage authStorage)
        {
            if (authStorage == null)
                throw new ArgumentNullException(nameof(authStorage));

            this._authStorage = authStorage;
            this._registry = new Dictionary<Type, RestApiUri>();

            this.Initialize();
        }

        private readonly IAuthStorage _authStorage;
        private readonly IDictionary<Type, RestApiUri> _registry;

        /// <summary>
        /// Executes the given Authentication and saves the
        /// result in the authentication storage.
        /// </summary>
        /// <param name="authentication"></param>
        public void Authenticate(IAuthentication authentication)
        {
            if (authentication == null)
                throw new ArgumentNullException(nameof(authentication));

            IAuthResult authResult = authentication.Authenticate();

            if (authResult == null)
                throw new AuthFailedExeption($"Authentication failed!");

            this._authStorage.Create(authResult);
        }

        /// <summary>
        /// Initializeses all of the services which is known
        /// for service gateway.
        /// </summary>
        public void Initialize()
        {
            this._registry.Add(typeof(UserService), new RestApiUri("http://localhost:25311/api/user"));
            this._registry.Add(typeof(MedicalChestService), new RestApiUri("http://localhost:25311/api/medicalchest"));
            this._registry.Add(typeof(ItemInformationService), new RestApiUri("http://localhost:25311/api/iteminformation"));
            this._registry.Add(typeof(ItemGroupInformationService), new RestApiUri("http://localhost:25311/api/itemgroupinformation"));
            this._registry.Add(typeof(MedicalChestInformationService), new RestApiUri("http://localhost:25311/api/medicalchestinformation"));
            this._registry.Add(typeof(ItemService), new RestApiUri("http://localhost:25311/api/item"));
        }

        /// <summary>
        /// Gets the required service.
        /// </summary>
        /// <typeparam name="TEntity">Entity which service to get.</typeparam>
        /// <typeparam name="TInterface">Interface of the service to return.</typeparam>
        /// <returns></returns>
        public TInterface GetService<TEntity, TInterface>()
            where TEntity : AbstractEntity where TInterface : IService<TEntity>
        {
            foreach (var registry in this._registry)
            {
                var cls = registry.Key;
                var host = registry.Value;

                if (cls.BaseType.GetGenericArguments()[0].Name.Equals(typeof(TEntity).Name))
                {
                    var ctor = cls.GetConstructors()[0];
                    var parameters = ctor.GetParameters();

                    if (parameters.Length == 1)
                        return (TInterface)Activator.CreateInstance(cls, host);
                    else
                    {
                        try
                        {
                            IAuthResult authResult = this._authStorage.Get(host.Host);
                            return (TInterface)Activator.CreateInstance(cls, host, authResult);
                        }
                        catch (DoNotExistException exception)
                        {
                            throw new AuthFailedExeption("");
                        }
                    }
                }
            }

            throw new ServiceGatewayExeption("Falied to find a matching service.");
        }

    }
}