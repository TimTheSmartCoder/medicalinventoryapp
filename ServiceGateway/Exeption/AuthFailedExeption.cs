﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceGateway.Exeption
{
    [Serializable]
    public class AuthFailedExeption : Exception
    {
        public AuthFailedExeption(String message) : base(message) { }
        public AuthFailedExeption(String message, Exception innerException) : base(message, innerException) { }
    }
}
