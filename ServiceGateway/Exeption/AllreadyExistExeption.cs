﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceGateway.Exeption
{
    [Serializable]
    public class AlreadyExistException : Exception
    {
        public AlreadyExistException(String message) : base(message) { }
        public AlreadyExistException(String message, Exception innerException) : base(message, innerException) { }
    }

}

