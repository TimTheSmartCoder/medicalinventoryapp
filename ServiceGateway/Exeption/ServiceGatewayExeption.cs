﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceGateway.Exeption
{
    [Serializable]
    public class ServiceGatewayExeption : Exception
    {
        public ServiceGatewayExeption(string message) : base(message) {}
        public ServiceGatewayExeption(string message, Exception innerException) : base(message, innerException) {}
    }
}
