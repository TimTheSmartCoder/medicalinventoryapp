﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceGateway.Exeption
{
    [Serializable]
    public class DoNotExistException : Exception
    {
        public DoNotExistException(string message) : base(message) { }
        public DoNotExistException(string message, Exception innerException) : base(message, innerException) { }
    }
}
