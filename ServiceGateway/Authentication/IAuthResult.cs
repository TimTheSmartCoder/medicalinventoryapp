﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ServiceGateway.Util;

namespace ServiceGateway.Authentication
{
    public interface IAuthResult
    {
        /// <summary>
        /// Returns the host of Authresult.
        /// </summary>
        /// <returns></returns>
        RestApiUri RestApiUri { get; }

        /// <summary>
        /// Authenticates the given client.
        /// </summary>
        /// <param name="client">The given client.</param>
        void Authenticate(HttpClient client);

        /// <summary>
        /// Returns true if valid, false if not.
        /// </summary>
        /// <returns></returns>
        bool IsValid();
    }
}
