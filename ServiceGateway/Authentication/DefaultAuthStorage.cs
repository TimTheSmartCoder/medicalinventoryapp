﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceGateway.Exeption;

namespace ServiceGateway.Authentication
{
    /// <summary>
    /// DefaultAuthStorage for storage authentication,
    /// in the service gateway.
    /// </summary>
    public class DefaultAuthStorage : IAuthStorage
    {
        /// <summary>
        /// Constructs a DefaultAuthStorage.
        /// </summary>
        public DefaultAuthStorage()
        {
            this._authResults = new List<IAuthResult>();
        }

        private IList<IAuthResult> _authResults;

        public IAuthResult Get(string host)
        {
            if (host == null)
                throw new ArgumentNullException(nameof(host));

            foreach (var authResult in this._authResults)
            {
                if (authResult.RestApiUri.Host.Equals(host))              
                    return authResult;               
            }

            return null;
        }

        public void Create(IAuthResult authResult)
        {          
            if (authResult == null)
                throw new ArgumentNullException(nameof(authResult));

            foreach (var result in this._authResults)
            {
                if (result.RestApiUri.Host.Equals(authResult.RestApiUri.Host))
                    throw new AlreadyExistException($"The following host: {authResult.RestApiUri.Host} is already authenticated.");
            }

            this._authResults.Add(authResult);
        }

        public bool IsValid()
        {
            if (this._authResults.Any() == false)
                return false;

            foreach (var authResult in this._authResults)
            {
                if (!authResult.IsValid())
                    return false;
            }

            return true;
        }
    }
}
