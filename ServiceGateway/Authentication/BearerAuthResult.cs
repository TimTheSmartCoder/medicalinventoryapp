﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ServiceGateway.Util;

namespace ServiceGateway.Authentication
{
    /// <summary>
    /// Bearer AuthResult, allows the service gateway to
    /// save the authentication.
    /// </summary>
    public class BearerAuthResult : IAuthResult
    {
        /// <summary>
        /// Constructs a BearerAuthResult from host and token.
        /// </summary>
        /// <param name="host">Host the AuthResult belongs to.</param>
        /// <param name="bearerToken">Authentication token for the service.</param>
        /// <param name="expiration">Expiration for the token.</param>
        public BearerAuthResult(RestApiUri host, string bearerToken, DateTime expiration)
        {
            if (host == null)
                throw new ArgumentNullException(nameof(host));
            if (bearerToken == null)
                throw new ArgumentNullException(nameof(bearerToken));
            if (expiration == null)
                throw new ArgumentNullException(nameof(expiration));

            this.RestApiUri = host;
            this._bearerToken = bearerToken;
            this._expiration = expiration;            
        }

        public RestApiUri RestApiUri { get; }
        private readonly string _bearerToken;
        private readonly DateTime _expiration;


        public void Authenticate(HttpClient client)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {this._bearerToken}");
        }

        public bool IsValid()
        {
            return (DateTime.Now <= this._expiration);
        }
    }
}
