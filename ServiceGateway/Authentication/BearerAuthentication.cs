﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using ServiceGateway.Util;

namespace ServiceGateway.Authentication
{
    /// <summary>
    /// BearerAuthentcation IAuthentication to allow
    /// authentication with Bearer token.
    /// </summary>
    public class BearerAuthentication : IAuthentication
    {
        /// <summary>
        /// Constructs a BearerAuthentication with username and password.
        /// </summary>
        /// <param name="userName">Username for the service.</param>
        /// <param name="password">Password for the service.</param>
        /// <param name="restApiUri">RestApiUri for the service.</param>
        public BearerAuthentication(string userName, string password, RestApiUri restApiUri)
        {
            if (userName == null)
                throw new ArgumentNullException(nameof(userName));
            if (password == null)
                throw new ArgumentNullException(nameof(password));
            if (restApiUri == null)
                throw new ArgumentNullException(nameof(restApiUri));

            this._restApiUri = restApiUri;
            this._password = password;
            this._username = userName;

        }

        private readonly string _username;
        private readonly string _password;
        private readonly RestApiUri _restApiUri;

        public IAuthResult Authenticate()
        {
            using (HttpClient client = new HttpClient())
            {
                //Create HttpClient to authentication.
                client.BaseAddress = new Uri($"{this._restApiUri.Host}/token");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );

                //Create form content for login authentication.
                var formContent = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", this._username),
                    new KeyValuePair<string, string>("password", this._password)
                });

                HttpResponseMessage response = client.PostAsync("/token", formContent).Result;

                if (response.IsSuccessStatusCode)
                {
                    //Get result.
                    string jsonResponse = response.Content.ReadAsStringAsync().Result;

                    //Parse json to json object.
                    var json = JObject.Parse(jsonResponse);

                    string token = json.GetValue("access_token").ToString();
                    DateTime expiration = Convert.ToDateTime(json.GetValue(".expires").ToString());

                    return new BearerAuthResult(this._restApiUri, token, expiration);
                }

                return null;
            }
        }
    }
}
