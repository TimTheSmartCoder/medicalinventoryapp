﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceGateway.Authentication
{
    public interface IAuthentication
    {
        /// <summary>
        /// Executes the authentication code and returns the authentication result
        /// </summary>
        /// <returns></returns>
        IAuthResult Authenticate();
    }
}
