﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceGateway.Authentication
{
    public interface IAuthStorage
    {
        /// <summary>
        /// Gets the AuthResult with the given host.
        /// </summary>
        /// <param name="host">Host of the AuthResult.</param>
        /// <returns></returns>
        IAuthResult Get(string host);

        /// <summary>
        /// Saves the given AuthResult.
        /// </summary>
        /// <param name="authResult">AuthResult to save.</param>
        void Create(IAuthResult authResult);

        /// <summary>
        /// Validates all of the AuthResult in the storage.
        /// </summary>
        /// <returns></returns>
        bool IsValid();
    }
}
