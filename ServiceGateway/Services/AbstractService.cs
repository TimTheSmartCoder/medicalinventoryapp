﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Entities;
using ServiceGateway.Authentication;
using ServiceGateway.Util;

namespace ServiceGateway.Services
{
    /// <summary>
    /// AbstractService provides the default actions
    /// for an service.
    /// </summary>
    /// <typeparam name="TEntity">Entity which service works with.</typeparam>
    public abstract class AbstractService<TEntity> : IService<TEntity> where TEntity : AbstractEntity
    {
        /// <summary>
        /// Constructs an AbstractService without authentication.
        /// </summary>
        /// <param name="restApiUri">RestApiUri to the server.</param>
        protected AbstractService(RestApiUri restApiUri) : this(restApiUri, null) {}

        /// <summary>
        /// Constructs an AbstractService which uses authentication.
        /// </summary>
        /// <param name="restApiUri">RestApiUri to the service.</param>
        /// <param name="authResult">AuthResult to use in Authentication.</param>
        protected AbstractService(RestApiUri restApiUri, IAuthResult authResult)
        {
            if (restApiUri == null)           
                throw new ArgumentNullException(nameof(restApiUri));

            this.RestApiUri = restApiUri;

            this.Client = new HttpClient();
            this.Client.BaseAddress = new Uri(this.RestApiUri.Host);
            this.Client.DefaultRequestHeaders.Accept.Clear();
            this.Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            authResult?.Authenticate(this.Client);
        }

        protected readonly HttpClient Client;
        public RestApiUri RestApiUri { get; }
    }
}
