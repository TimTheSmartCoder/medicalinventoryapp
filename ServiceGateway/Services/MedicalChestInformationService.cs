﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using ServiceGateway.Util;
using ServiceGateway.Authentication;

namespace ServiceGateway.Services
{
    public class MedicalChestInformationService : AbstractDefaultService<MedicalChestInformation>
    {
        public MedicalChestInformationService(RestApiUri restApiUri, IAuthResult authResult) : base(restApiUri, authResult)
        {
        }
    }
}
