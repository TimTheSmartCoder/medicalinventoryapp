﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using ServiceGateway.Authentication;
using ServiceGateway.Util;

namespace ServiceGateway.Services
{
    class ItemInformationService : AbstractDefaultService<ItemInformation>
    {
        public ItemInformationService(RestApiUri restApiUri, IAuthResult authResult) : base(restApiUri, authResult)
        {
        }
    }
}
