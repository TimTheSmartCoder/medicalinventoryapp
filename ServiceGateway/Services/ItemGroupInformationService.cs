﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using ServiceGateway.Authentication;
using ServiceGateway.Util;

namespace ServiceGateway.Services
{
    class ItemGroupInformationService : AbstractDefaultService<ItemGroupInformation>
    {
        public ItemGroupInformationService(RestApiUri restApiUri, IAuthResult authResult) : base(restApiUri, authResult)
        {
        }
    }
}
