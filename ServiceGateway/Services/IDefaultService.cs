﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace ServiceGateway.Services
{
    public interface IDefaultService<TEntity> 
        : IService<TEntity> where TEntity : AbstractEntity
    {
        /// <summary>
        /// Returns one entity with the given id.
        /// </summary>
        /// <param name="id">The id of the entity.</param>
        /// <returns></returns>
        TEntity Get(int id);

        /// <summary>
        /// Returns every entity of the type.
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> GetAll();

        /// <summary>
        /// Creates a new entity based on the given entity.
        /// </summary>
        /// <param name="entity">The given entity.</param>
        /// <returns></returns>
        TEntity Create(TEntity entity);

        /// <summary>
        /// Updates the entity with the new entity.
        /// </summary>
        /// <param name="entity">The new entity.</param>
        /// <returns></returns>
        TEntity Update(TEntity entity);

        /// <summary>
        /// Deletes the entity matching the given entity.
        /// </summary>
        /// <param name="entity">The given entity.</param>
        /// <returns></returns>
        TEntity Delete(TEntity entity);

        /// <summary>
        /// Pagination method, to get the data in pages, to
        /// prevent loading all the data.
        /// </summary>
        /// <param name="pageIndex">Index of the paged data.</param>
        /// <param name="pageSize">Size of a page.</param>
        /// <returns></returns>
        IEnumerable<TEntity> Pagination(int pageIndex, int pageSize);

        /// <summary>
        /// Counts all the entities, there exists in the database
        /// for the specific entity.
        /// </summary>
        /// <returns></returns>
        int Count();
    }
}
