﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Entities;
using ServiceGateway.Authentication;
using ServiceGateway.Exeption;
using ServiceGateway.Util;

namespace ServiceGateway.Services
{
    public class UserService : AbstractDefaultService<User>, IUserService
    {      
        public UserService(RestApiUri restApiUri, IAuthResult authResult) : base(restApiUri, authResult) {}


        public User GetFromUserName(string userName)
        {
            HttpResponseMessage httpResponseMessage
                = this.Client.GetAsync($"{this.RestApiUri.AbsolutePath}/getbyusername?username={userName}").Result;

            if (httpResponseMessage.IsSuccessStatusCode)
                return httpResponseMessage.Content.ReadAsAsync<User>().Result;

            throw new ServiceGatewayExeption($"Failed to get the Entity {typeof(User).Name} with {this.RestApiUri.AbsolutePath}");
        }

        public User GetFromEmail(string email)
        {
            HttpResponseMessage httpResponseMessage
                = this.Client.GetAsync($"{this.RestApiUri.AbsolutePath}?email={email}").Result;

            if (httpResponseMessage.IsSuccessStatusCode)
                return httpResponseMessage.Content.ReadAsAsync<User>().Result;

            throw new ServiceGatewayExeption($"Failed to get the Entity {typeof(User).Name} with {this.RestApiUri.AbsolutePath}");
        }

        public User GetAuthenticatedUser()
        {
            HttpResponseMessage httpResponseMessage
                = this.Client.GetAsync($"{this.RestApiUri.AbsolutePath}/getauthenticateduser").Result;

            if (httpResponseMessage.IsSuccessStatusCode)
                return httpResponseMessage.Content.ReadAsAsync<User>().Result;

            throw new ServiceGatewayExeption($"Failed to get the Entity {typeof(User).Name} with {this.RestApiUri.AbsolutePath}");
        }
    }
}
