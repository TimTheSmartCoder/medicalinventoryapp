﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Entities;
using ServiceGateway.Authentication;
using ServiceGateway.Exeption;
using ServiceGateway.Util;

namespace ServiceGateway.Services
{
    /// <summary>
    /// AbstractDefaultService provides the default CRUD functionality
    /// and a little more and service.
    /// </summary>
    /// <typeparam name="TEntity">Entity which the service is using.</typeparam>
    public abstract class AbstractDefaultService<TEntity> 
        : AbstractService<TEntity>, IDefaultService<TEntity> where TEntity : AbstractEntity
    {
        /// <summary>
        /// Constructs a DefaultService without authentication.
        /// </summary>
        /// <param name="restApiUri">RestApiUri to service.</param>
        protected AbstractDefaultService(RestApiUri restApiUri) : base(restApiUri) {}

        /// <summary>
        /// Constructs a DefaultService with authentication.
        /// </summary>
        /// <param name="restApiUri">RestApiUri to the service.</param>
        /// <param name="authResult">AuthResult which should be used to authenticate.</param>
        protected AbstractDefaultService(RestApiUri restApiUri, IAuthResult authResult) : base(restApiUri, authResult) {}

        public virtual TEntity Get(int id)
        {
            HttpResponseMessage httpResponseMessage 
                = this.Client.GetAsync($"{this.RestApiUri.AbsolutePath}/get?id={id}").Result;

            if (httpResponseMessage.IsSuccessStatusCode)
                return httpResponseMessage.Content.ReadAsAsync<TEntity>().Result;
            
            throw new ServiceGatewayExeption($"Failed to get the Entity {typeof(TEntity).Name} with {this.RestApiUri.AbsolutePath}");
        }

        public IEnumerable<TEntity> GetAll()
        {
            HttpResponseMessage httpResponseMessage
                = this.Client.GetAsync($"{this.RestApiUri.AbsolutePath}/getall/").Result;

            if (httpResponseMessage.IsSuccessStatusCode)
                return httpResponseMessage.Content.ReadAsAsync<IEnumerable<TEntity>>().Result;

            throw new ServiceGatewayExeption($"Failed to get the Entities {typeof(TEntity).Name} with {this.RestApiUri.AbsolutePath}");
        }

        public TEntity Create(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            HttpResponseMessage httpResponseMessage
                = this.Client.PostAsJsonAsync($"{this.RestApiUri.AbsolutePath}/create/", entity).Result;

            if (httpResponseMessage.IsSuccessStatusCode)
                return httpResponseMessage.Content.ReadAsAsync<TEntity>().Result;

            throw new ServiceGatewayExeption($"Failed to create the Entity {typeof(TEntity).Name} with {this.RestApiUri.AbsolutePath}");
        }

        public TEntity Update(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            HttpResponseMessage httpResponseMessage
                = this.Client.PutAsJsonAsync($"{this.RestApiUri.AbsolutePath}/update/", entity).Result;

            if (httpResponseMessage.IsSuccessStatusCode)
                return httpResponseMessage.Content.ReadAsAsync<TEntity>().Result;

            throw new ServiceGatewayExeption($"Failed to update the Entity {typeof(TEntity).Name} with {this.RestApiUri.AbsolutePath}");

        }

        public TEntity Delete(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            HttpResponseMessage httpResponseMessage
                = this.Client.DeleteAsync($"{this.RestApiUri.AbsolutePath}/delete?id={entity.Id}").Result;

            if (httpResponseMessage.IsSuccessStatusCode)
                return httpResponseMessage.Content.ReadAsAsync<TEntity>().Result;

            throw new ServiceGatewayExeption($"Failed to delete the Entity {typeof(TEntity).Name} with {this.RestApiUri.AbsolutePath}");
        }

        public IEnumerable<TEntity> Pagination(int pageIndex, int pageSize)
        {
            HttpResponseMessage httpResponseMessage
                = this.Client.GetAsync($"{this.RestApiUri.AbsolutePath}/pagination/{pageIndex}/{pageSize}").Result;

            if (httpResponseMessage.IsSuccessStatusCode)
                return httpResponseMessage.Content.ReadAsAsync<IEnumerable<TEntity>>().Result;

            throw new ServiceGatewayExeption($"Failed to get the Entities {typeof(TEntity).Name} with {this.RestApiUri.AbsolutePath}");
        }

        public int Count()
        {
            HttpResponseMessage httpResponseMessage
                = this.Client.GetAsync($"{this.RestApiUri.AbsolutePath}/count").Result;

            if (httpResponseMessage.IsSuccessStatusCode)
                return httpResponseMessage.Content.ReadAsAsync<int>().Result;

            throw new ServiceGatewayExeption($"Failed to get count of {typeof(TEntity).Name} with {this.RestApiUri.AbsolutePath}");
        }
    }
}
