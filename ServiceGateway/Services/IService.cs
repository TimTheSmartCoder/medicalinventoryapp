﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace ServiceGateway.Services
{
    public interface IService<TEntity> where TEntity : AbstractEntity
    {
        //Only for verification...
    }
}
