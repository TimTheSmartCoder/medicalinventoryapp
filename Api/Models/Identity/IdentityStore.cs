﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Backend;
using Backend.Exceptions;
using Backend.Repositories;
using Entities;
using Microsoft.AspNet.Identity;

namespace Api.Models.Identity
{
    public class IdentityStore : IUserStore<IdentityUser, int>, IUserPasswordStore<IdentityUser, int>, IUserEmailStore<IdentityUser, int>
    {
        public IdentityStore()
        {
            this._userRepository = RepositoryFacade.GetUserManager();
        }

        private readonly IUserRepository _userRepository;

        public async Task CreateAsync(IdentityUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            
            this._userRepository.Create(AutoMap(user));
        }

        public async Task UpdateAsync(IdentityUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            this._userRepository.Update(AutoMap(user));
        }

        public async Task DeleteAsync(IdentityUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            this._userRepository.Delete(AutoMap(user));
        }

        public Task<IdentityUser> FindByIdAsync(int userId)
        {
            try
            {
                User user = this._userRepository.Get(userId);
                return Task.FromResult<IdentityUser>(AutoMap(user));
            }
            catch (DoNotExistException exception)
            {
                return Task.FromResult<IdentityUser>(null);
            }
        }

        public Task<IdentityUser> FindByNameAsync(string userName)
        {
            if (userName == null)
                throw new ArgumentNullException(nameof(userName));

            try
            {
                User user = this._userRepository.GetFromUserName(userName);
                return Task.FromResult<IdentityUser>(AutoMap(user));
            }
            catch (DoNotExistException exception)
            {
                return Task.FromResult<IdentityUser>(null);
            }
        }

        public User AutoMap(IdentityUser identityUser)
        {
            User user = AutoMapper.Mapper.Map<User>(identityUser);
            user.Password = identityUser.PasswordHash;
            return user;
        }

        public IdentityUser AutoMap(User user)
        {
            IdentityUser identityUser = AutoMapper.Mapper.Map<IdentityUser>(user);
            identityUser.PasswordHash = user.Password;
            return identityUser;;
        }

        public void Dispose()
        {
            //Nothing to dispose.
        }

        public async Task SetPasswordHashAsync(IdentityUser user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
        }

        public Task<string> GetPasswordHashAsync(IdentityUser user)
        {
            return Task.FromResult<string>(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(IdentityUser user)
        {
            return Task.FromResult<bool>(user.PasswordHash != null);
        }

        public async Task SetEmailAsync(IdentityUser user, string email)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            if (email == null)
                throw new ArgumentNullException(nameof(email));

            user.Email = email;
        }

        public Task<string> GetEmailAsync(IdentityUser user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            return Task.FromResult<string>(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(IdentityUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetEmailConfirmedAsync(IdentityUser user, bool confirmed)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityUser> FindByEmailAsync(string email)
        {
            if (email == null)
                throw new ArgumentNullException(nameof(email));

            try
            {
                User user = this._userRepository.GetFromEmail(email);
                return Task.FromResult<IdentityUser>(AutoMap(user));
            }
            catch (DoNotExistException exception)
            {
                return Task.FromResult<IdentityUser>(null);
            }
        }
    }
}