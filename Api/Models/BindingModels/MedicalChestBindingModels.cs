﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entities;

namespace Api.Models.BindingModels
{
    public class MedicalChestBindingModel
    {
        public User User { get; set; }
        public MedicalChestInformation MedicalChestInformation { get; set; }
        public List<ItemGroup> ItemGroups { get; set; }
    }
}