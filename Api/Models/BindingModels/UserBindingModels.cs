﻿using System.ComponentModel.DataAnnotations;
using Entities;

namespace Api.Models.BindingModels
{
    public class RegisterBindingModel
    {
        [Required]
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password{ get; set; }       
    }
}