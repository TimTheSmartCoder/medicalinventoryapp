﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Api.Models.BindingModels;
using Newtonsoft.Json;

namespace Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.Re‌​ferenceLoopHandling = ReferenceLoopHandling.Ignore;

            //AutoMapper configuration begin.
            AutoMapper.Mapper.Initialize(config =>
            {
                config
                    .CreateMap
                    <Api.Models.Identity.IdentityUser, Entities.User>();
                config
                    .CreateMap
                    <Entities.User, Api.Models.Identity.IdentityUser>();
                config
                    .CreateMap
                    <Api.Models.BindingModels.RegisterBindingModel, Api.Models.Identity.IdentityUser>();
                config
                    .CreateMap<Entities.User, Api.Models.BindingModels.RegisterBindingModel>();
                config
                    .CreateMap<Api.Models.BindingModels.MedicalChestBindingModel, Entities.MedicalChest>();
                config
                    .CreateMap<Entities.MedicalChest, Api.Models.BindingModels.MedicalChestBindingModel>();
            });
            //AutoMapper configuration end.
        }
    }
}
