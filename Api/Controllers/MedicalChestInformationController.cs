using System;
﻿using Backend;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Api.Models.BindingModels;
using Backend;
using Entities;

namespace Api.Controllers
{
    public class MedicalChestInformationController : ApiController
    {
        public MedicalChestInformationController()
        {
            this._medicalChestInformationRepository = RepositoryFacade.GetMedicalChestInformationManager();
        }

        private IRepository<MedicalChestInformation> _medicalChestInformationRepository;

        [HttpGet]
        [Authorize]
        [ResponseType(typeof(MedicalChestInformation))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(this._medicalChestInformationRepository.Get(id));
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Authorize]
        [ResponseType(typeof(IEnumerable<MedicalChestInformation>))]
        public IHttpActionResult GetAll()
        {
            try
            {
                return Ok(this._medicalChestInformationRepository.GetAll());
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [Authorize]
        [ResponseType(typeof(MedicalChestInformation))]
        public IHttpActionResult Create(MedicalChestInformation model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            try
            {
                MedicalChestInformation medicalChest
                    = AutoMapper.Mapper.Map<MedicalChestInformation>(model);
                return Ok(this._medicalChestInformationRepository.Create(medicalChest));
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpPut]
        [Authorize]
        [ResponseType(typeof(MedicalChest))]
        public IHttpActionResult Update(MedicalChestInformation model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            try
            {
                MedicalChestInformation medicalChest
                    = AutoMapper.Mapper.Map<MedicalChestInformation>(model);
                return Ok(this._medicalChestInformationRepository.Update(medicalChest));
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Authorize]
        [ResponseType(typeof(MedicalChestInformation))]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var medChestInfo = this._medicalChestInformationRepository.Get(id);

                foreach (var chest in medChestInfo.MedicalChests)
                {
                    RepositoryFacade.GetMedicalChestManager().Delete(chest);
                }

                return Ok(this._medicalChestInformationRepository.Delete(new MedicalChestInformation() { Id = id }));
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

    }
}
