﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Api.Models;
using Api.Models.Identity;
using Backend;
using Backend.Repositories;
using Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using RegisterBindingModel = Api.Models.BindingModels.RegisterBindingModel;

namespace Api.Controllers
{
    public class UserController : ApiController
    {

        public UserController()
        {
            this._userRepository = RepositoryFacade.GetUserManager();
        }

        private readonly IUserRepository _userRepository;

        [HttpGet]
        [Authorize]
        [ResponseType(typeof(User))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(this._userRepository.Get(id));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Authorize]
        [ResponseType(typeof(IEnumerable<User>))]
        public IHttpActionResult GetAll()
        {
            try
            {
                return Ok(this._userRepository.GetAll());
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost]
        public IHttpActionResult Create(RegisterBindingModel model)
        {
            try
            {
                if (model == null)
                    return BadRequest();

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                IdentityUser user = AutoMapper.Mapper.Map<IdentityUser>(model);
                
                    //Try to create the user witht the identity framework.
                    IdentityResult result =  
                    Request.GetOwinContext()
                    .GetUserManager<ApplicationUserManager>()
                    .CreateAsync(user, model.Password).Result;

                if (!result.Succeeded)
                    return BadRequest();

                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpPut]
        [Authorize]
        [ResponseType(typeof(User))]
        public IHttpActionResult Update(User user)
        {
            try
            {
                //Prevent overriding of id and username.
                var nUser = this._userRepository.Get(user.Id);

                user.Id = nUser.Id;
                user.UserName = user.UserName;
                user.Admin = nUser.Admin;

                return Ok(_userRepository.Update(user));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Authorize]
        [ResponseType(typeof(User))]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var user = this._userRepository.Get(id);

                if (user != null)
                    RepositoryFacade.GetMedicalChestManager().Delete(user.MedicalChest);

                return Ok(_userRepository.Delete(new User() {Id = id}));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Authorize]
        [ResponseType(typeof(User))]
        public IHttpActionResult GetByUsername(string username)
        {
            try
            {
                return Ok(_userRepository.GetFromUserName(username));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Authorize]
        [ResponseType(typeof(User))]
        public IHttpActionResult GetByEmail(string email)
        {
            try
            {
                return Ok(_userRepository.GetFromEmail(email));
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [Route("api/User/pagination/{pageIndex}/{pageSize}")]
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(IEnumerable<User>))]
        public IHttpActionResult Pagination(int pageIndex, int pageSize)
        {
            try
            {
                return Ok(_userRepository.Pagination(pageIndex, pageSize));
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [Route("api/User/count")]
        [HttpGet]
        [ResponseType(typeof(int))]
        public IHttpActionResult Count()
        {
            try
            {
                return Ok(_userRepository.Count());
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Authorize]
        [ResponseType(typeof(User))]
        public IHttpActionResult GetAuthenticatedUser()
        {
            User user = this._userRepository.GetFromUserName(this.User.Identity.Name);
            return Ok(user);         
        }
    }
}
