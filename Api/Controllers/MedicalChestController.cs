﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Api.Models.BindingModels;
using Backend;
using Entities;

namespace Api.Controllers
{
    public class MedicalChestController : ApiController
    {
        public MedicalChestController()
        {
            this._medicalChestRepository = RepositoryFacade.GetMedicalChestManager();
        }

        private IRepository<MedicalChest> _medicalChestRepository;

        [HttpGet]
        [Authorize]
        [ResponseType(typeof(MedicalChest))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(this._medicalChestRepository.Get(id));
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Authorize]
        [ResponseType(typeof(IEnumerable<MedicalChest>))]
        public IHttpActionResult GetAll()
        {
            try
            {
                return Ok(this._medicalChestRepository.GetAll());
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [Authorize]
        [ResponseType(typeof(MedicalChest))]
        public IHttpActionResult Create(MedicalChestBindingModel model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            try
            {
                MedicalChest medicalChest
                    = AutoMapper.Mapper.Map<MedicalChest>(model);
                return Ok(this._medicalChestRepository.Create(medicalChest));
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        //[HttpPut]
        //[Authorize]
        //[ResponseType(typeof(MedicalChest))]
        //public IHttpActionResult Update(MedicalChest model)
        //{
        //    if (model == null)
        //        throw new ArgumentNullException(nameof(model));

        //    try
        //    {
        //        List<int> track = new List<int>();

        //        foreach(var group in model.ItemGroups)
        //            foreach(var item in group.Items)
        //            {
        //                if (track.Contains(item.Id))
        //                    throw new Exception("FUCK");
        //                track.Add(item.Id);
        //            }

        //        return Ok(this._medicalChestRepository.Update(model));
        //    }
        //    catch (Exception exception)
        //    {
        //        return BadRequest();
        //    }
        //}

        [HttpPut]
        [Authorize]
        [ResponseType(typeof(MedicalChest))]
        public IHttpActionResult Update(MedicalChest chest)
        {
            if (chest == null)
                throw new ArgumentNullException(nameof(chest));

            try
            {
                return Ok(this._medicalChestRepository.Update((MedicalChest)chest));
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Authorize]
        [ResponseType(typeof(MedicalChest))]
        public IHttpActionResult Delete(int id)
        {
            try
            {


                return Ok(this._medicalChestRepository.Delete(new MedicalChest() {Id = id}));
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }
    }
}
