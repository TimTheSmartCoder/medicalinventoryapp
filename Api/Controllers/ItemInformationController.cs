﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using Backend;
using Entities;

namespace Api.Controllers
{
    public class ItemInformationController : ApiController
    {
        /// <summary>
        /// Constructs ItemInformationController.
        /// </summary>
        public ItemInformationController()
        {
            this._itemInformationRepository = RepositoryFacade.GetItemInformationManager();
        }

        private readonly IRepository<ItemInformation> _itemInformationRepository;

        /// <summary>
        /// Gets the ItemInformation witht the given id.
        /// </summary>
        /// <param name="id">Id of the ItemInformation to get.</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(ItemInformation))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(this._itemInformationRepository.Get(id));
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Gets all the ItemInfromaton.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ResponseType(typeof(IEnumerable<ItemInformation>))]
        public IHttpActionResult GetAll()
        {
            try
            {
                return Ok(this._itemInformationRepository.GetAll());
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [Authorize]
        [ResponseType(typeof(ItemInformation))]
        public IHttpActionResult Create(ItemInformation itemInformation)
        {
            if (itemInformation == null)
                return BadRequest();

            ItemInformationLang daLang = itemInformation.Information("da-DK");
            ItemInformationLang enLang = itemInformation.Information("en");

            try
            {
                ItemInformation old = new ItemInformation();
                ItemInformationLang oldDaLang = new ItemInformationLang() {Locale = "da-DK"};
                ItemInformationLang oldEnLang = new ItemInformationLang() { Locale = "en" };
                old.ItemInformationLangs = new List<ItemInformationLang>()
                {
                    oldDaLang,
                    oldEnLang
                };

                oldDaLang.Administration = daLang.Administration;
                oldEnLang.Administration = enLang.Administration;
                oldDaLang.Mfag = daLang.Mfag;
                oldEnLang.Mfag = enLang.Mfag;
                oldDaLang.Name = daLang.Name;
                oldEnLang.Name = enLang.Name;

                old.AtcCode = itemInformation.AtcCode;
                old.ItemGroupInformation = new ItemGroupInformation() {Id = itemInformation.ItemGroupInformation.Id};

                this._itemInformationRepository.Create(old);

                return Ok(old);
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpPut]
        [Authorize]
        [ResponseType(typeof(ItemInformation))]
        public IHttpActionResult Update(ItemInformation itemInformation)
        {
            if (itemInformation == null)
                return BadRequest();

            ItemInformationLang daLang = itemInformation.Information("da-DK");
            ItemInformationLang enLang = itemInformation.Information("en");

            try
            {
                ItemInformation old = this._itemInformationRepository.Get(itemInformation.Id);
                ItemInformationLang oldDaLang = old.Information("da-DK");
                ItemInformationLang oldEnLang = old.Information("en");

                oldDaLang.Administration = daLang.Administration;
                oldEnLang.Administration = enLang.Administration;
                oldDaLang.Mfag = daLang.Mfag;
                oldEnLang.Mfag = enLang.Mfag;
                oldDaLang.Name = daLang.Name;
                oldEnLang.Name = enLang.Name;

                old.AtcCode = itemInformation.AtcCode;

                this._itemInformationRepository.Update(old);

                return Ok(old);
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Authorize]
        [ResponseType(typeof(ItemInformation))]
        public IHttpActionResult Delete(int? id)
        {
            if (id == null)
                return BadRequest();
            try
            {
                var item = this._itemInformationRepository.Get(id.GetValueOrDefault());

                foreach (var itemItem in item.Items)
                {
                    RepositoryFacade.GetItemManager().Delete(itemItem);
                }

                this._itemInformationRepository.Delete(item);

                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }
    }
}
