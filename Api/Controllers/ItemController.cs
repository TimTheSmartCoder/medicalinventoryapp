﻿using Backend;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Api.Controllers
{
    public class ItemController : ApiController
    {
        public ItemController()
        {
            this._itemRepository = RepositoryFacade.GetItemManager();
        }
        private readonly IRepository<Item> _itemRepository;

        [HttpGet]
        [Authorize]
        [ResponseType(typeof(Item))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                return Ok(this._itemRepository.Get(id));
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Authorize]
        [ResponseType(typeof(IEnumerable<Item>))]
        public IHttpActionResult GetAll()
        {
            try
            {
                return Ok(this._itemRepository.GetAll());
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpPut]
        [Authorize]
        [ResponseType(typeof(ItemInformation))]
        public IHttpActionResult Update(Item item)
        {
            if (item == null)
                return BadRequest();

            try
            {
                Item old = this._itemRepository.Get(item.Id);

                old.ExpDateOne = item.ExpDateOne;
                old.ExpDateTwo = item.ExpDateTwo;
                old.ExpDateThree = item.ExpDateThree;
                old.TotalStock = item.TotalStock;

                this._itemRepository.Update(old);

                return Ok(old);
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }
    }
}
