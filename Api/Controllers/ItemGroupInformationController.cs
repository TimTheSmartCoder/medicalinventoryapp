﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Backend;
using Entities;

namespace Api.Controllers
{
    public class ItemGroupInformationController : ApiController
    {
        public ItemGroupInformationController()
        {
            this._itemGroupInformationRepository = RepositoryFacade.GetItemGroupInformationManager();
        }

        private readonly IRepository<ItemGroupInformation> _itemGroupInformationRepository;

        [HttpGet]
        [Authorize]
        [ResponseType(typeof(ItemInformation))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var item = this._itemGroupInformationRepository.Get(id);
                return Ok(item);
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Authorize]
        [ResponseType(typeof(IEnumerable<ItemGroupInformation>))]
        public IHttpActionResult GetAll()
        {
            try
            {
                return Ok(this._itemGroupInformationRepository.GetAll());
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpPut]
        [Authorize]
        [ResponseType(typeof(ItemInformation))]
        public IHttpActionResult Update(ItemGroupInformation itemGroupInformation)
        {
            if (itemGroupInformation == null)
                return BadRequest();

            ItemGroupInformationLang daLang = itemGroupInformation.Information("da-DK");
            ItemGroupInformationLang enLang = itemGroupInformation.Information("en");

            try
            {
                ItemGroupInformation old = this._itemGroupInformationRepository.Get(itemGroupInformation.Id);
                ItemGroupInformationLang oldDaLang = old.Information("da-DK");
                ItemGroupInformationLang oldEnLang = old.Information("en");

                oldDaLang.Name = daLang.Name;
                oldEnLang.Name = enLang.Name;


                this._itemGroupInformationRepository.Update(old);

                return Ok(old);
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [Authorize]
        [ResponseType(typeof(ItemInformation))]
        public IHttpActionResult Create(ItemGroupInformation itemGroupInformation)
        {
            if (itemGroupInformation == null)
                return BadRequest();

            ItemGroupInformationLang daLang = itemGroupInformation.Information("da-DK");
            ItemGroupInformationLang enLang = itemGroupInformation.Information("en");

            try
            {
                ItemGroupInformation old = new ItemGroupInformation();
                ItemGroupInformationLang oldDaLang = new ItemGroupInformationLang() { Locale = "da-DK" };
                ItemGroupInformationLang oldEnLang = new ItemGroupInformationLang() { Locale = "en" };
                old.ItemGroupInformationLangs = new List<ItemGroupInformationLang>()
                {
                    oldDaLang,
                    oldEnLang
                };

                oldDaLang.Name = daLang.Name;
                oldEnLang.Name = enLang.Name;

                old.MedicalChestInformation = itemGroupInformation.MedicalChestInformation;


                this._itemGroupInformationRepository.Create(old);

                return Ok(old);
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Authorize]
        [ResponseType(typeof(ItemInformation))]
        public IHttpActionResult Delete(int? id)
        {
            if (id == null)
                return BadRequest();
            try
            {
                var item = this._itemGroupInformationRepository.Get(id.GetValueOrDefault());

                foreach (var itemItemGroup in item.ItemGroups)
                {
                    RepositoryFacade.GetItemGroupRepository().Delete(itemItemGroup);
                }

                this._itemGroupInformationRepository.Delete(item);
                return Ok(item);
            }
            catch (Exception exception)
            {
                return BadRequest();
            }
        }
    }
}
